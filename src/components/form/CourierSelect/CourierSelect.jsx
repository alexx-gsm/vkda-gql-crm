import React from 'react'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'

import { useQuery } from '@apollo/react-hooks'
import { GET_COURIERS } from '../../../graphql/query'

function CourierSelect({ courier, setCourier }) {
  const [inputValue, setInputValue] = React.useState('')

  const { data } = useQuery(GET_COURIERS)

  const couriers = data?.couriers?.data || []

  return (
    <Autocomplete
      value={couriers.find(c => c._id === courier?._id) || null}
      onChange={(_, newValue) => setCourier(newValue)}
      id='select-courier'
      inputValue={inputValue}
      onInputChange={(_, newInputValue) => {
        setInputValue(newInputValue)
      }}
      options={couriers}
      getOptionLabel={option => option?.title || ''}
      renderInput={params => (
        <TextField {...params} label='Курьер' variant='standard' />
      )}
      noOptionsText={'Не найдено'}
      fullWidth
    />
  )
}

export default CourierSelect
