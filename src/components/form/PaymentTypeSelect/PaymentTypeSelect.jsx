import React from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

import useStyles from './styles';

function PaymentTypeSelect({
  paymentType,
  setPaymentType,
  variant = 'standard',
  margin = 'normal',
}) {
  const classes = useStyles();

  return (
    <FormControl
      margin={margin}
      variant={variant}
      fullWidth
      className={classes.formControl}
    >
      <InputLabel htmlFor='paymentType'>Тип оплаты</InputLabel>
      <Select
        label='Тип оплаты'
        value={paymentType}
        onChange={setPaymentType}
        inputProps={{
          name: 'paymentType',
          id: 'paymentType',
        }}
      >
        <MenuItem value='cash'>Наличные</MenuItem>
        <MenuItem value='card'>Карта</MenuItem>
        <MenuItem value='account'>Договор</MenuItem>
      </Select>
    </FormControl>
  );
}

export default PaymentTypeSelect;
