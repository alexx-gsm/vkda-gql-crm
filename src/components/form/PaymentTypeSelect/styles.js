import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(() => ({
  ErrorHelperText: {
    position: 'absolute',
    top: '100%',
    marginTop: 0,
  },
}))
