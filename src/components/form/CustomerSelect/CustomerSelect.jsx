import React from 'react';
import TextField from '@material-ui/core/TextField';
import { Grid, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { useQuery } from '@apollo/react-hooks';
import { GET_CUSTOMERS } from '../../../graphql/query';
import useStyles from './styles';

function CustomerSelect({ customer, setCustomer }) {
  const classes = useStyles();

  const [inputValue, setInputValue] = React.useState('');

  const { data, loading } = useQuery(GET_CUSTOMERS);
  const customers = data?.customers?.data || [];

  if (loading) {
    return <Skeleton />;
  }

  const getOptionLabel = (label) =>
    `${label?.company?.title || 'ф/л'}, ${label?.name} `;

  const renderOption = (option) => (
    <Grid container direction='column'>
      <Grid container spacing={1} alignItems='center'>
        <Grid item>
          <Typography variant='h6'>
            {option?.company?.title || 'ф.л.'}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant='caption'>
            {`${option?.company?.address || ''} ${option?.address}`}
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={1} alignItems='center'>
        <Grid item>
          <Typography variant='subtitle1'>{option?.name}</Typography>
        </Grid>
        <Grid item>
          <Typography variant='subtitle2'>{option?.phone}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );

  return (
    <Grid container direction='column'>
      <Autocomplete
        value={customer}
        onChange={(_, newValue) => {
          setCustomer(newValue);
        }}
        id='select-customer'
        inputValue={inputValue}
        onInputChange={(_, newInputValue) => {
          setInputValue(newInputValue);
        }}
        options={customers}
        getOptionLabel={getOptionLabel}
        renderOption={renderOption}
        classes={{ root: classes.Root, input: classes.Input }}
        renderInput={(params) => (
          <TextField {...params} label='Клиент' variant='standard' />
        )}
        noOptionsText={'Не найдено'}
        fullWidth
      />
      <Grid container spacing={2}>
        <Grid item>
          {(customer?.company?.address || customer?.address) && (
            <Typography variant='h6' classes={{ root: classes.TypoAddress }}>
              <em>адрес: </em>
              {`${customer?.company?.address || ''} ${customer?.address || ''}`}
            </Typography>
          )}
        </Grid>
        <Grid item>
          {customer?.phone && (
            <Typography variant='h6' classes={{ root: classes.TypoAddress }}>
              <em>тел.: </em>
              {`${customer?.phone}`}
            </Typography>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
}

export default CustomerSelect;
