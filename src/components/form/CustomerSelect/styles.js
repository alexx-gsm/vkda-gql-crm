import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Root: {},
  Input: {
    fontWeight: theme.typography.fontWeightMedium,
    fontSize: '1.5rem',
    color: theme.palette.primary.dark,
  },
  TypoAddress: {
    marginTop: theme.spacing(1),
    fontWeight: theme.typography.fontWeightLight,
    color: theme.palette.error.dark,
    '& em': {
      fontSize: '16px',
      marginRight: theme.spacing(1),
      color: theme.palette.grey[500],
      textDecoration: 'underline',
    },
  },
}));
