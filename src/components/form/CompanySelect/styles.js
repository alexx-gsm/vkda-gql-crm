import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  ErrorHelperText: {
    position: 'absolute',
    top: '100%',
    marginTop: 0,
  },
  Select: {
    minHeight: '49px',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  MenuItem: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
}));
