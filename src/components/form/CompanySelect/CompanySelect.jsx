import React from 'react';
// @material-ui
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
} from '@material-ui/core';
import { FormHelperText } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

import { useQuery } from '@apollo/react-hooks';
import { GET_COMPANIES } from '../../../graphql/query/companiesQuery';
// styles
import useStyles from './styles';

function CompanySelect({
  company,
  setCompany,
  variant = 'outlined',
  margin = 'normal',
  error,
  errorText,
}) {
  const classes = useStyles();
  const { data, loading } = useQuery(GET_COMPANIES);

  if (loading) {
    return <Skeleton />;
  }
  const companies = data.companies.data;

  return (
    <FormControl
      disabled={companies.length === 0}
      variant={variant}
      margin={margin}
      fullWidth
      className={classes.formControl}
      error={error}
    >
      <InputLabel htmlFor='company'>Организация</InputLabel>
      <Select
        value={companies.find((c) => c._id === company._id) || ''}
        onChange={setCompany}
        label='Организация'
        inputProps={{
          name: 'company',
          id: 'company',
        }}
        classes={{ root: classes.Select }}
      >
        <MenuItem value=''>
          <em>-</em>
        </MenuItem>
        {companies.map((item) => {
          return (
            <MenuItem
              key={item._id}
              value={item}
              classes={{ root: classes.MenuItem }}
            >
              <Typography variant='subtitle1'>{item.title}</Typography>
              <Typography variant='caption'>{item.address}</Typography>
            </MenuItem>
          );
        })}
      </Select>
      {errorText && (
        <FormHelperText classes={{ root: classes.ErrorHelperText }}>
          {errorText}
        </FormHelperText>
      )}
    </FormControl>
  );
}

export default CompanySelect;
