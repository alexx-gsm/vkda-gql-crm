import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Actions: {
    padding: theme.spacing(1, 3),
  },
}));
