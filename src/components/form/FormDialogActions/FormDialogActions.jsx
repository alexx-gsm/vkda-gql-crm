import React from 'react';
import { Button, DialogActions } from '@material-ui/core';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';

import useStyles from './styles';

function FormDialogActions({ loading, handleSubmit, handleClose }) {
  const classes = useStyles();
  return (
    <DialogActions classes={{ root: classes.Actions }}>
      <Button onClick={handleClose}>Отмена</Button>
      <Button
        onClick={handleSubmit}
        color='primary'
        variant='contained'
        disabled={loading}
        endIcon={loading ? <HourglassEmptyIcon /> : <ArrowRightAltIcon />}
      >
        Сохранить
      </Button>
    </DialogActions>
  );
}

export default FormDialogActions;
