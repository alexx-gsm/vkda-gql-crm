import React from 'react';
import { IconButton } from '@material-ui/core';
import TodayIcon from '@material-ui/icons/Today';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { DatePicker } from '@material-ui/pickers';

import moment from 'moment';
import MomentUtils from '@date-io/moment';

function FormDatePicker({ date, handleDateChange }) {
  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <DatePicker
        format='D MMMM YYYY'
        value={moment(date) || moment()}
        onChange={handleDateChange}
        animateYearScrolling
        showTodayButton
        todayLabel='Сегодня'
        cancelLabel='Отмена'
        okLabel='Выбрать'
        size='small'
        InputProps={{
          endAdornment: (
            <IconButton>
              <TodayIcon />
            </IconButton>
          ),
        }}
      />
    </MuiPickersUtilsProvider>
  );
}

export default FormDatePicker;
