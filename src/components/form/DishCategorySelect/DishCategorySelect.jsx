import React from 'react';
// @material-ui
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { FormHelperText } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

import { useQuery } from '@apollo/react-hooks';
import { GET_CATEGORIES } from '../../../graphql/query/categories';
// styles
import useStyles from './styles';

function DishCategorySelect({
  category,
  setCategory,
  variant = 'outlined',
  margin = 'normal',
  error,
  errorText,
}) {
  const classes = useStyles();
  const { data, loading } = useQuery(GET_CATEGORIES);

  if (loading) {
    return <Skeleton />;
  }
  const categories = data.categories.data;

  return (
    <FormControl
      disabled={categories.length === 0}
      variant={variant}
      margin={margin}
      fullWidth
      className={classes.formControl}
      error={error}
    >
      <InputLabel htmlFor='category'>Категория</InputLabel>
      <Select
        value={category}
        onChange={setCategory}
        label='Категория'
        inputProps={{
          name: 'category',
          id: 'category',
        }}
      >
        <MenuItem value=''>
          <em>-</em>
        </MenuItem>
        {categories.map(({ title, _id }) => {
          return (
            <MenuItem key={_id} value={_id}>
              {title}
            </MenuItem>
          );
        })}
      </Select>
      {errorText && (
        <FormHelperText classes={{ root: classes.ErrorHelperText }}>
          {errorText}
        </FormHelperText>
      )}
    </FormControl>
  );
}

export default DishCategorySelect;
