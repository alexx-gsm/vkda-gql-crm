import React from 'react';
import { Typography } from '@material-ui/core';

import useStyles from './styles';

function FormDialogTitle({ title }) {
  const classes = useStyles();

  return (
    <Typography variant='h4' align='center' classes={{ root: classes.Title }}>
      {title}
    </Typography>
  );
}

export default FormDialogTitle;
