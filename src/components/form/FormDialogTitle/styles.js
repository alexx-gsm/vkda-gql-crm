import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Title: {
    textTransform: 'uppercase',
    color: theme.palette.primary.dark,
  },
}));
