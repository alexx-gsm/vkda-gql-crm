import React from 'react';
import { TextField, InputAdornment } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

function SearchInput({ value, handleChange }) {
  return (
    <TextField
      value={value}
      onChange={handleChange}
      InputProps={{
        endAdornment: getIcon(),
      }}
    />
  );

  function getIcon() {
    return (
      <InputAdornment position='end'>
        <SearchIcon fontSize='small' />
      </InputAdornment>
    );
  }
}

export default SearchInput;
