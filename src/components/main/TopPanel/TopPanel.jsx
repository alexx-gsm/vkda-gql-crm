import React from 'react';
import { AppBar, Toolbar, Avatar } from '@material-ui/core';
import { Box, Typography, IconButton } from '@material-ui/core';
import { Menu, MenuItem } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import useLocalStorage from '../../../hooks/useLocalStorage';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { IS_LOGGED_IN } from '../../../graphql/query';

import clsx from 'clsx';
import useStyles from './styles';
import { Redirect } from 'react-router-dom';

function TopPanel({ isOpen, setIsOpen }) {
  const classes = useStyles();

  const client = useApolloClient();
  const { data: cacheResult, loading: cacheLoading } = useQuery(IS_LOGGED_IN);
  const [, setToken] = useLocalStorage('token');

  // dropdown menu
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const onMenuClose = () => setAnchorEl(null);

  const onSignOut = async () => {
    onMenuClose();
    setToken('');
    await client.writeQuery({
      query: IS_LOGGED_IN,
      data: {
        isLoggedIn: false,
        user: null,
      },
    });
  };

  if (cacheResult.isLoading) {
    return '';
  }

  if (!cacheLoading && !cacheResult.isLoggedIn) {
    return <Redirect to='/signin' />;
  }

  const { isLoggedIn, user } = !cacheLoading && cacheResult ? cacheResult : {};

  return (
    <AppBar
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isOpen,
      })}
    >
      <Toolbar>
        <IconButton
          color='inherit'
          aria-label='open drawer'
          onClick={() => setIsOpen(true)}
          edge='start'
          className={clsx(classes.menuButton, {
            [classes.hide]: isOpen,
          })}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant='h6' className={classes.title}>
          CRM
        </Typography>
        {isLoggedIn && user && (
          <Box classes={{ root: classes.Box }}>
            <Typography>{user.name || user.email}</Typography>
            <IconButton
              aria-label='account of current user'
              aria-controls='menu-appbar'
              aria-haspopup='true'
              onClick={handleMenu}
              color='inherit'
            >
              <Avatar>{user.acronym}</Avatar>
            </IconButton>
            <Menu
              id='menu-appbar'
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorEl)}
              onClose={onMenuClose}
            >
              <MenuItem onClick={onMenuClose}>Profile</MenuItem>
              <MenuItem onClick={onSignOut}>Выйти</MenuItem>
            </Menu>
          </Box>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default TopPanel;
