import React from 'react';
import clsx from 'clsx';
import { NavLink, Redirect } from 'react-router-dom';
import { Drawer, Fade } from '@material-ui/core';
import { Grid, Avatar, Button } from '@material-ui/core';
import { List, ListItem, ListItemText, ListItemIcon } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import { useQuery } from '@apollo/react-hooks';
import useLocalStorage from '../../../hooks/useLocalStorage';
import { IS_LOGGED_IN } from '../../../graphql/query';

import links from './links';
import useStyles from './styles';

function LeftPanel({ isOpen, setIsOpen }) {
  const classes = useStyles();

  const [, setToken] = useLocalStorage('token');
  const { data, loading: cacheLoading, client } = useQuery(IS_LOGGED_IN);
  const { isLoggedIn, user } = data || {};

  const handleDrawerClose = () => setIsOpen(false);
  const handleDrawerOpen = () => setIsOpen(true);

  const onSignOut = async () => {
    setToken('');
    await client.writeQuery({
      query: IS_LOGGED_IN,
      data: {
        isLoggedIn: false,
        user: null,
      },
    });
  };

  if (!cacheLoading && !isLoggedIn) {
    return <Redirect to='/signin' />;
  }

  return (
    <Drawer
      variant='permanent'
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: isOpen,
        [classes.drawerClose]: !isOpen,
      })}
      classes={{
        paper: clsx(
          {
            [classes.drawerOpen]: isOpen,
            [classes.drawerClose]: !isOpen,
          },
          classes.DrawerPaper,
        ),
      }}
    >
      <List classes={{ root: classes.List }}>{links.map(getLink)}</List>
      {isLoggedIn && user && (
        <Grid
          container
          wrap='nowrap'
          justify='space-between'
          classes={{ root: classes.GridAvatar }}
        >
          <Avatar>{user.acronym}</Avatar>
          {isOpen && (
            <Fade in={isOpen} timeout={1000}>
              <Button
                onClick={onSignOut}
                variant='contained'
                color='secondary'
                startIcon={<ExitToAppIcon />}
              >
                Выход
              </Button>
            </Fade>
          )}
        </Grid>
      )}
      <div className={classes.toolbar}>
        {isOpen ? (
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        ) : (
          <IconButton onClick={handleDrawerOpen}>
            <ChevronRightIcon />
          </IconButton>
        )}
      </div>
    </Drawer>
  );

  function getLink({ to, title, Icon, exact }) {
    return (
      <ListItem
        key={to}
        button
        component={NavLink}
        to={to}
        exact={exact}
        classes={{ root: classes.NavLink }}
      >
        <ListItemIcon>
          <Icon />
        </ListItemIcon>
        <ListItemText primary={title} />
      </ListItem>
    );
  }
}

export default LeftPanel;
