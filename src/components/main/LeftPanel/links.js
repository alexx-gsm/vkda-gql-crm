// import DashboardIcon from '@material-ui/icons/Dashboard';
import AppleIcon from '@material-ui/icons/Apple';
import AssignmentIcon from '@material-ui/icons/Assignment';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
// import DirectionsBikeIcon from '@material-ui/icons/DirectionsBike';
// import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
// import HomeWorkIcon from '@material-ui/icons/HomeWork';
// import RoomServiceIcon from '@material-ui/icons/RoomService';

export default [
  // {
  //   to: '/',
  //   title: 'Dashboard',
  //   Icon: DashboardIcon,
  //   exact: true,
  // },
  {
    to: '/menu',
    title: 'Меню',
    Icon: AssignmentIcon,
  },
  {
    to: '/dishes',
    title: 'Блюда',
    Icon: AppleIcon,
  },
  {
    to: '/categories',
    title: 'Категории',
    Icon: LocalOfferIcon,
  },
  // {
  //   to: '/couriers',
  //   title: 'Курьеры',
  //   Icon: DirectionsBikeIcon,
  // },
  // {
  //   to: '/customers',
  //   title: 'Клиенты',
  //   Icon: AssignmentIndIcon,
  // },
  // {
  //   to: '/companies',
  //   title: 'Организации',
  //   Icon: HomeWorkIcon,
  // },
  // {
  //   to: '/orders',
  //   title: 'Заказы',
  //   Icon: RoomServiceIcon,
  // },
];
