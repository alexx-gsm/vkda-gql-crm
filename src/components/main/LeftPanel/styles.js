import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 240;

export default makeStyles((theme) => ({
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(7) + 3,
    },
  },
  DrawerPaper: {
    overflow: 'hidden',
    border: 0,
  },
  List: {
    marginBottom: 'auto',
    padding: 0,
  },
  GridAvatar: {
    padding: theme.spacing(0, 1),
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    height: '50px',
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  NavLink: {
    '&.active': {
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    '&.active svg': {
      color: theme.palette.common.white,
    },
  },
}));
