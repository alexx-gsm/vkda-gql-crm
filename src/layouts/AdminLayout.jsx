import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import LeftPanel from '../components/main/LeftPanel/LeftPanel';
import useStyles from './styles';

function AdminLayout({ children }) {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* <TopPanel isOpen={isOpen} setIsOpen={setIsOpen} /> */}
      <LeftPanel isOpen={isOpen} setIsOpen={setIsOpen} />
      <main className={classes.content}>{children}</main>
    </div>
  );
}

export default AdminLayout;
