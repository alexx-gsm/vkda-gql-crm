import { observable, action, decorate, computed } from 'mobx'
import moment from 'moment'

const emptyOrder = {
  number: '',
  date: moment(),
  customer: null,
  paymentType: 'cash',
  discount: '0',
  address: '',
  courier: '',
  comment: '',
  dishes: [],
}

const emptyFilters = {
  searchFilter: '',
}

class OrderStore {
  isOpenDialog = false
  order = emptyOrder
  filters = emptyFilters

  get getTotalPrice() {
    if (!this.order.dishes.length) {
      return null
    }

    return this.order.dishes.reduce((acc, row) => {
      return acc + row.dish.price * row.count
    }, 0)
  }

  openDialog = () => {
    this.isOpenDialog = true
  }

  closeDialog = () => {
    this.isOpenDialog = false
    this.order = emptyOrder
  }

  setValue = key => e => {
    const value = e?.target?.value || e
    this.order[key] = value
  }

  setFilter = key => filter => (this.filters[key] = filter)
  clearFilter = () => (this.filters = emptyFilters)
}

decorate(OrderStore, {
  isOpenDialog: observable,
  order: observable,
  filters: observable,
  getTotalPrice: computed,
  openDialog: action,
  closeDialog: action,
  setValue: action,
  setFilter: action,
  clearFilters: action,
})

export default OrderStore
