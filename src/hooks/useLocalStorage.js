import { useState, useEffect } from 'react';

const APP_LOCAL_TOKEN = 'VKDALocalAppState';

const getLocalStorageObject = () => {
  const appLocalStorage = localStorage.getItem(APP_LOCAL_TOKEN);

  return appLocalStorage ? JSON.parse(appLocalStorage) : {};
};

const getValueByKey = (key) => {
  const objAppLocalStorage = getLocalStorageObject();

  return objAppLocalStorage ? objAppLocalStorage[key] : null;
};

const setValueByKey = (key, value) => {
  const objAppLocalStorage = getLocalStorageObject();

  localStorage.setItem(APP_LOCAL_TOKEN, JSON.stringify({ ...objAppLocalStorage, [key]: value }));
};

export default (key, initialValue = '') => {
  const [value, setValue] = useState(getValueByKey(key) || initialValue);

  useEffect(() => {
    setValueByKey(key, value);
  }, [key, value]);

  return [value, setValue];
};
