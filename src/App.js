import React from 'react';
import Pages from './pages';

import { useQuery } from '@apollo/react-hooks';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './themes/vkdaColorTheme';

import useLocalStorage from './hooks/useLocalStorage';
import { CHECK_TOKEN, IS_LOGGED_IN } from './graphql/query';

import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru');

// TODO: remove checking-token-logic to useAuth() hook
function App() {
  const [token, setToken] = useLocalStorage('token');

  const { client } = useQuery(CHECK_TOKEN, {
    variables: { token },
    fetchPolicy: 'network-only',
    onCompleted: ({ checkToken }) => {
      client.writeData({
        data: {
          isLoading: false,
          isLoggedIn: !!checkToken,
          user: checkToken,
        },
      });
      setToken(token);
    },
  });

  const { data: cacheData, loading: cacheLoading } = useQuery(IS_LOGGED_IN);

  if (cacheLoading || cacheData.isLoading) {
    return <h5>loading...</h5>;
  }

  return (
    <ThemeProvider theme={theme}>
      <Pages />
    </ThemeProvider>
  );
}

export default App;

//   "proxy": "http://localhost:8008"
