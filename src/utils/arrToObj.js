export const arrToObj = (arr) => {
  if (Array.isArray(arr)) {
    return arr.reduce((acc, item) => {
      return {
        ...acc,
        [item.label]: item.value,
      };
    }, {});
  }
  return arr;
};
