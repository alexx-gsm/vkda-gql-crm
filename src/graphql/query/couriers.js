import gql from 'graphql-tag';

export const GET_COURIERS = gql`
  query GetCouriers {
    couriers {
      isSuccess
      data {
        _id
        title
        comment
      }
      errors {
        label
        value
      }
      code
      message
    }
  }
`;
