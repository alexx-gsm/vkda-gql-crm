import gql from 'graphql-tag';

export const IS_LOGGED_IN = gql`
  query IsUserLoggedIn {
    isLoggedIn @client
    isLoading @client
    user @client {
      email
      name
      acronym
    }
  }
`;
