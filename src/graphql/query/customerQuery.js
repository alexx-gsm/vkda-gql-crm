import gql from 'graphql-tag';

export const GET_CUSTOMERS = gql`
  query GetCustomers {
    customers {
      isSuccess
      data {
        _id
        name
        company {
          _id
          title
          address
        }
        paymentType
        discount
        address
        phone
        courier {
          _id
          title
        }
        comment
      }
      errors {
        label
        value
      }
      code
      message
    }
  }
`;
