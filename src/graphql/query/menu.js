import gql from 'graphql-tag';
import { MENU_DISH_FIELDS } from '../fragments/menuDishFields';

export const GET_MENUS = gql`
  query GetMenus($week: Int) {
    menus(week: $week) {
      isSuccess
      data {
        _id
        date
        isActive
        dishes {
          _id
          ...menuDishFields
        }
      }
      message
      errors {
        label
        value
      }
      code
    }
  }
  ${MENU_DISH_FIELDS}
`;

export const GET_MENU_BY_DATE = gql`
  query GetMenu($date: Date!) {
    menuByDate(date: $date) {
      isSuccess
      data {
        _id
        date
        isActive
        dishes {
          _id
          ...menuDishFields
        }
      }
      message
      errors {
        label
        value
      }
      code
    }
  }
  ${MENU_DISH_FIELDS}
`;
