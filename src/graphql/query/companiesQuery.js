import gql from 'graphql-tag';

export const GET_COMPANIES = gql`
  query GetCompanies {
    companies {
      isSuccess
      data {
        _id
        title
        address
        comment
      }
      errors {
        label
        value
      }
      code
      message
    }
  }
`;
