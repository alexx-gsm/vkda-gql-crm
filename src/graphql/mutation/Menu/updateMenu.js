import gql from 'graphql-tag';
import { MENU_DISH_FIELDS } from '../../fragments/menuDishFields';

export const UPDATE_MENU = gql`
  mutation updateMenu(
    $_id: ID
    $date: String!
    $dishes: [MenuDishDataInput]!
    $isActive: Boolean!
  ) {
    updateMenu(
      input: { _id: $_id, date: $date, dishes: $dishes, isActive: $isActive }
    ) {
      isSuccess
      data {
        _id
        date
        isActive
        dishes {
          _id
          ...menuDishFields
        }
      }
      message
      errors {
        label
        value
      }
    }
  }
  ${MENU_DISH_FIELDS}
`;
