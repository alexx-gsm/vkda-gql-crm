import gql from 'graphql-tag';

export const UPDATE_COMPANY = gql`
  mutation updateCompany(
    $_id: ID
    $title: String!
    $address: String!
    $comment: String
  ) {
    updateCompany(
      input: { _id: $_id, title: $title, address: $address, comment: $comment }
    ) {
      isSuccess
      data {
        _id
        title
        address
        comment
      }
      message
      errors {
        label
        value
      }
    }
  }
`;
