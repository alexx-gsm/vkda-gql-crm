import gql from 'graphql-tag';
import { CATEGORY_FIELDS } from '../../fragments/categoryFields';

export const EDIT_CATEGORY = gql`
  mutation editCategory(
    $_id: ID
    $title: String!
    $parent: String
    $sorting: String
    $description: String
  ) {
    editCategory(
      input: {
        _id: $_id
        title: $title
        parent: $parent
        sorting: $sorting
        description: $description
      }
    ) {
      isSuccess
      data {
        _id
        ...CategoryFields
      }
      message
      errors {
        label
        value
      }
    }
  }
  ${CATEGORY_FIELDS}
`;
