import gql from 'graphql-tag';

export const UPDATE_CUSTOMER = gql`
  mutation updateCustomer(
    $_id: ID
    $name: String!
    $company: String
    $paymentType: String
    $discount: String
    $address: String
    $phone: String!
    $courier: String
    $comment: String
  ) {
    updateCustomer(
      input: {
        _id: $_id
        name: $name
        company: $company
        paymentType: $paymentType
        discount: $discount
        address: $address
        phone: $phone
        courier: $courier
        comment: $comment
      }
    ) {
      isSuccess
      data {
        _id
        name
        company {
          _id
          title
          address
        }
        paymentType
        discount
        address
        phone
        courier {
          _id
          title
        }
        comment
      }
      message
      errors {
        label
        value
      }
    }
  }
`;
