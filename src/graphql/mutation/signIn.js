import gql from 'graphql-tag';

export const SIGNIN_USER = gql`
  mutation signIn($email: String!, $password: String!) {
    signIn(input: { email: $email, password: $password }) {
      isSuccess
      data {
        token
        user {
          email
          name
          role
          acronym
        }
      }
      errors {
        label
        value
      }
    }
  }
`;
