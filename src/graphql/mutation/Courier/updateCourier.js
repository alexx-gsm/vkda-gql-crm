import gql from 'graphql-tag';

export const UPDATE_COURIER = gql`
  mutation updateCourier($_id: ID, $title: String!, $comment: String) {
    updateCourier(input: { _id: $_id, title: $title, comment: $comment }) {
      isSuccess
      data {
        _id
        title
        comment
      }
      message
      errors {
        label
        value
      }
    }
  }
`;
