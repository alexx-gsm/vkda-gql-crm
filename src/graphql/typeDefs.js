import gql from 'graphql-tag';

export const typeDefs = gql`
  extend type Query {
    isLoading: Boolean!
    isLoggedIn: Boolean!
    checkToken(token: String!): User
  }
`;
