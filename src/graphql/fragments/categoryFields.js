import gql from 'graphql-tag';

export const CATEGORY_FIELDS = gql`
  fragment CategoryFields on Category {
    title
    parent
    sorting
    description
  }
`;
