import gql from 'graphql-tag';

export const MENU_DISH_FIELDS = gql`
  fragment menuDishFields on MenuDish {
    index
    title
    category {
      _id
      title
      sorting
    }
    weight
    price
    composition
    comment
  }
`;
