import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';

const MODULE_NAME = `${APP_NAME}/COURIER`;

// constants
const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;

const CourierPageContext = React.createContext();

const emptyCourier = {
  title: '',
  comment: '',
};

const ReducerRecord = Record({
  isOpenDialog: false,
  courier: emptyCourier,
});

/**
 ** reducer
 */
function courierPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state.set('isOpenDialog', false).set('courier', emptyCourier);
    }
    case UPDATE_RECORD: {
      return state.setIn(['courier', payload.key], payload.value);
    }
    case SET_ITEM: {
      return state.set('courier', payload.courier).set('isOpenDialog', true);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 ** provider
 */
function CourierPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    courierPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <CourierPageContext.Provider value={value} {...props} />;
}

/**
 ** context
 */
function useCourierPageContext() {
  const context = React.useContext(CourierPageContext);
  if (!context) {
    throw new Error(
      `useCourierPageContext must be used within a CourierPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateCourier = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const storeCourier = (courier) =>
    dispatch({ type: SET_ITEM, payload: { courier } });

  return {
    state,
    openDialog,
    closeDialog,
    updateCourier,
    storeCourier,
  };
}

export { CourierPageProvider, useCourierPageContext };
