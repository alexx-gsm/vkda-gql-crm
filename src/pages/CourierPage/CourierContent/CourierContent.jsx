import React from 'react';
import { Table, TableContainer, TableHead, TableBody } from '@material-ui/core';
import { TableRow, TableCell } from '@material-ui/core';
import { Paper, Typography } from '@material-ui/core';
import {
  StyledTableCell,
  StyledTableRow,
} from '../../../components/table/StyledTable.jsx';

import { useQuery } from '@apollo/react-hooks';
import { useCourierPageContext } from '../courierPageContext';
import { GET_COURIERS } from '../../../graphql/query';

const getTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell>Название</StyledTableCell>
      </TableRow>
    </TableHead>
  );
};

function CourierContent() {
  const { storeCourier } = useCourierPageContext();

  const { data, loading } = useQuery(GET_COURIERS);

  if (loading) {
    return (
      <Typography variant='h6' align='center'>
        loading...
      </Typography>
    );
  }

  const couriers = data?.couriers?.data || [];

  const handleClick = (courier) => () => {
    storeCourier(courier);
  };

  return (
    <TableContainer component={Paper}>
      <Table size='small'>
        {getTableHead()}
        <TableBody>
          {couriers
            .sort((a, b) => new Date(a.date) - new Date(b.date))
            .map((courier) => {
              return (
                <StyledTableRow
                  key={courier._id}
                  onClick={handleClick(courier)}
                >
                  <TableCell style={{ width: '100%' }}>
                    <Typography variant='h5'>{courier.title}</Typography>
                  </TableCell>
                </StyledTableRow>
              );
            })}
        </TableBody>
      </Table>
      {couriers?.length === 0 && (
        <Typography variant='h6' align='center'>
          Нет данных
        </Typography>
      )}
    </TableContainer>
  );
}

export default CourierContent;
