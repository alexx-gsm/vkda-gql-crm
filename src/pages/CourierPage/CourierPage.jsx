import React from 'react';
import { Container } from '@material-ui/core';
import CourierPanel from './CourierPanel';
import CourierContent from './CourierContent';
import CourierDialog from './CourierDialog';

import { CourierPageProvider } from './courierPageContext';

function CourierPage() {
  return (
    <Container maxWidth='md' disableGutters>
      <CourierPageProvider>
        <CourierPanel />
        <CourierContent />
        <CourierDialog />
      </CourierPageProvider>
    </Container>
  );
}

export default CourierPage;
