import React from 'react';
import { Grid, Button, Typography } from '@material-ui/core';

import { useCourierPageContext } from '../courierPageContext';

import useStyles from './styles';

function CourierPanel() {
  const classes = useStyles();
  const { openDialog } = useCourierPageContext();

  return (
    <Grid
      container
      alignItems='flex-end'
      spacing={2}
      classes={{ root: classes.GridRoot }}
    >
      <Grid item>
        <Button variant='contained' color='secondary' onClick={openDialog}>
          Добавить
        </Button>
      </Grid>
      <Grid item xs>
        <Typography variant='h4' align='right'>
          Курьеры
        </Typography>
      </Grid>
    </Grid>
  );
}

export default CourierPanel;
