import React from 'react';
import { TextField } from '@material-ui/core';
import { Dialog, DialogContent } from '@material-ui/core';
import FormDialogActions from '../../../components/form/FormDialogActions';
import FormDialogTitle from '../../../components/form/FormDialogTitle';

import { useMutation } from '@apollo/react-hooks';
import { useCourierPageContext } from '../courierPageContext';
import { UPDATE_COURIER } from '../../../graphql/mutation';
import { GET_COURIERS } from '../../../graphql/query';

import { arrToObj } from '../../../utils/arrToObj';
import useStyles from './styles';

function CourierDialog() {
  const classes = useStyles();
  const [error, setError] = React.useState({});
  const { state, closeDialog, updateCourier } = useCourierPageContext();
  const { isOpenDialog, courier } = state;
  const [update, { loading }] = useMutation(UPDATE_COURIER, {
    onCompleted: onCompletedMutationHandler,
    update: updateCache,
    onError: (error) => console.log('edit courier error:', error),
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    update({ variables: courier });
  };

  return (
    <Dialog
      maxWidth='sm'
      open={isOpenDialog}
      classes={{ paper: classes.Paper }}
    >
      <FormDialogTitle title='Курьер' />
      <DialogContent classes={{ root: classes.Content }}>
        <TextField
          autoFocus
          required
          id='title'
          label='Название'
          margin='normal'
          variant='outlined'
          fullWidth
          value={courier.title || ''}
          onChange={updateCourier('title')}
          error={!!error?.title}
          helperText={error?.title || ''}
          FormHelperTextProps={{ classes: { root: classes.ErrorHelperText } }}
        />
        <TextField
          id='comment'
          label='Комментарий'
          margin='normal'
          variant='outlined'
          multiline
          rows={3}
          fullWidth
          value={courier.comment || ''}
          onChange={updateCourier('comment')}
          error={!!error?.comment}
          helperText={error?.comment || ''}
          FormHelperTextProps={{
            classes: { root: classes.ErrorHelperText },
          }}
        />
      </DialogContent>
      <FormDialogActions
        loading={loading}
        handleSubmit={handleSubmit}
        handleClose={closeDialog}
      />
    </Dialog>
  );

  function onCompletedMutationHandler({ updateCourier }) {
    if (updateCourier.isSuccess) {
      closeDialog();
    } else {
      if (updateCourier.errors.length) {
        setError(arrToObj(updateCourier.errors));
      }
    }
  }

  function updateCache(cache, { data }) {
    if (courier._id || !data.updateCourier.isSuccess) {
      return;
    }
    const items = cache.readQuery({ query: GET_COURIERS });
    const couriers = items.couriers ? items.couriers.data : [];

    cache.writeQuery({
      query: GET_COURIERS,
      data: {
        couriers: {
          ...items.couriers,
          data: [...couriers, ...data.updateCourier.data],
        },
      },
    });
  }
}

export default CourierDialog;
