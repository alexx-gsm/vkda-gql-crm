import React, { useState } from 'react';
import { Container, TextField, Button, Typography } from '@material-ui/core';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';

import useLocalStorage from '../../hooks/useLocalStorage';
import { useMutation, useQuery } from '@apollo/react-hooks';

import { SIGNIN_USER } from '../../graphql/mutation';
import { IS_LOGGED_IN } from '../../graphql/query';
import useStyles from './styles';
import { Redirect } from 'react-router-dom';

// TODO: add using useAuth() hook to check token on Mount Component
function Login() {
  const classes = useStyles();
  const [, setToken] = useLocalStorage('token');

  const { data: loggedInData, loading: loggedInLoading } = useQuery(
    IS_LOGGED_IN,
  );

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [formError, setFormErrors] = useState([]);

  const [login, { loading, error }] = useMutation(SIGNIN_USER, {
    update: async (cache, { data: { signIn } }) => {
      if (signIn.isSuccess) {
        const { token, user } = signIn.data[0];
        await cache.writeQuery({
          query: IS_LOGGED_IN,
          data: {
            isLoggedIn: true,
            user,
          },
        });
        setToken(token);
      } else {
        await cache.writeQuery({
          query: IS_LOGGED_IN,
          data: {
            isLoggedIn: false,
            user: null,
          },
        });
        setToken('');
        setFormErrors(
          signIn.errors.reduce((acc, error) => {
            return {
              ...acc,
              [error.label]: error.value,
            };
          }, {}),
        );
      }
    },
    onError(error) {
      console.log('login error', error);
    },
  });

  if (loggedInLoading) {
    return <p>login loading ...</p>;
  }

  if (loggedInData && loggedInData.isLoggedIn) {
    return <Redirect to='/' />;
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    login({ variables: { email, password } });
  };

  if (error) {
    return <h3>error occur</h3>;
  }

  return (
    <Container maxWidth='xs'>
      <Typography variant='h2'>Вход</Typography>
      <form noValidate autoComplete='off' onSubmit={handleSubmit}>
        <TextField
          required
          id='email'
          label='Email'
          margin='normal'
          fullWidth
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          error={!!(formError && formError.email)}
          helperText={formError && formError.email}
        />
        <TextField
          required
          type='password'
          id='password'
          label='Password'
          margin='normal'
          fullWidth
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          error={!!(formError && formError.password)}
          helperText={formError && formError.password}
        />
        <Button
          classes={{ root: classes.Button }}
          variant='contained'
          color='primary'
          type='submit'
          disabled={loading || loggedInLoading}
          endIcon={loading ? <HourglassEmptyIcon /> : <ArrowRightAltIcon />}
        >
          Войти
        </Button>
      </form>
    </Container>
  );
}

export default Login;
