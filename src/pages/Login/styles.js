import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Button: {
    marginTop: theme.spacing(2),
  },
}));
