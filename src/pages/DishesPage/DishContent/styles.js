import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  TableContainer: {
    borderRadius: 0,
    maxWidth: '1200px',
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  GridPaging: {
    marginTop: 'auto',
    marginBottom: theme.spacing(8),
  },
}));
