import React from 'react';
import { Table, TableContainer, TableBody } from '@material-ui/core';
import { TableCell } from '@material-ui/core';
import { Grid, Paper, Typography } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import Pagination from '@material-ui/lab/Pagination';
import { StyledTableRow } from '../../../components/table/StyledTable.jsx';
import DishContentTableHeadRow from '../components/DishContentTableHeadRow';

import { useDishPageContext } from '../dishPageContext';

import { useQuery } from '@apollo/react-hooks';
import formatter from '../../../utils/formatter';
import { GET_DISHES } from '../../../graphql/query';
import useStyles from './styles';

const ITEMS_PER_PAGE = 10;

function DishContent() {
  const classes = useStyles();

  const { data, loading } = useQuery(GET_DISHES);
  const { state, storeDish } = useDishPageContext();

  const { categoryFilter, searchFilter } = state.filter;

  const [page, setPage] = React.useState(1);
  const handleChange = (event, value) => {
    setPage(value);
  };

  React.useEffect(() => {
    setPage(1);
  }, [categoryFilter, searchFilter]);

  if (loading) {
    return (
      <Typography variant='h6' align='center'>
        loading...
      </Typography>
    );
  }

  const dishes = getFilteredDishes();
  const count = Math.ceil(dishes.length / ITEMS_PER_PAGE);

  const handleRowClick = (dish) => () => {
    storeDish({
      ...dish,
      category: dish.category._id,
    });
  };

  return (
    <TableContainer
      component={Paper}
      classes={{ root: classes.TableContainer }}
    >
      <Table size='small'>
        <DishContentTableHeadRow />
        <TableBody>
          {getSortedDishes().map((dish) => (
            <StyledTableRow key={dish._id} onClick={handleRowClick(dish)}>
              <TableCell style={{ width: '100%' }}>
                <Typography variant='h5'>{dish.title}</Typography>
              </TableCell>
              <TableCell align='center'>
                {dish.isComplex && <CheckIcon color='primary' />}
              </TableCell>
              <TableCell>
                <Typography noWrap variant='caption'>
                  {dish.category.title}
                </Typography>
              </TableCell>
              <TableCell>{dish.weight}</TableCell>
              <TableCell align='right'>
                <Typography noWrap variant='h6'>
                  {formatter.format(dish.price)}
                </Typography>
              </TableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
      {dishes.length === 0 && (
        <Typography variant='h6' align='center'>
          Нет данных
        </Typography>
      )}

      {count > 1 && (
        <Grid container justify='center' classes={{ root: classes.GridPaging }}>
          <Pagination
            count={count}
            page={page}
            onChange={handleChange}
            variant='outlined'
            color='secondary'
          />
        </Grid>
      )}
    </TableContainer>
  );

  function getFilteredDishes() {
    const categoryFilteredDishes = categoryFilter
      ? data.dishes.data.filter((d) => d.category._id === categoryFilter)
      : data.dishes.data;

    return searchFilter
      ? categoryFilteredDishes.filter(
          (dish) =>
            dish.title.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1,
        )
      : categoryFilteredDishes;
  }

  function getSortedDishes() {
    return dishes
      .sort((a, b) => (a.title.toLowerCase() > b.title.toLowerCase() ? 1 : -1))
      .slice(ITEMS_PER_PAGE * (page - 1), ITEMS_PER_PAGE * page);
  }
}

export default DishContent;
