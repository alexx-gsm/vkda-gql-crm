import React from 'react';
import { TableHead, TableRow, TableCell } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

function DishContentTableHeadRow() {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell>Название</StyledTableCell>
        <StyledTableCell>Составное</StyledTableCell>
        <StyledTableCell>Категория</StyledTableCell>
        <StyledTableCell>Вес</StyledTableCell>
        <StyledTableCell>Цена</StyledTableCell>
      </TableRow>
    </TableHead>
  );
}

export default DishContentTableHeadRow;
