import React from 'react';
import { TextField, Grid } from '@material-ui/core';
import { Dialog, DialogContent } from '@material-ui/core';
import { FormControlLabel, Switch } from '@material-ui/core';
import DishCategorySelect from '../../../components/form/DishCategorySelect';
import FormDialogActions from '../../../components/form/FormDialogActions';
import FormDialogTitle from '../../../components/form/FormDialogTitle';

import { useMutation } from '@apollo/react-hooks';
import { useDishPageContext } from '../dishPageContext';
import { UPDATE_DISH } from '../../../graphql/mutation';
import { GET_DISHES } from '../../../graphql/query';

import { arrToObj } from '../../../utils/arrToObj';
import useStyles from './styles';

function DishDialog() {
  const classes = useStyles();
  const [error, setError] = React.useState({});
  const {
    state: { isOpenDialog, dish },
    closeDialog,
    updateDish,
    setValue,
  } = useDishPageContext();
  const [update, { loading }] = useMutation(UPDATE_DISH, {
    onCompleted: onCompletedMutationHandler,
    update: updateCache,
    onError: (error) => console.log('edit dish error:', error),
  });

  const handleSwitch = (field) => (e) => setValue(field)(e.target.checked);

  const handleSubmit = (e) => {
    e.preventDefault();
    update({ variables: dish });
  };

  return (
    <Dialog
      maxWidth='sm'
      open={isOpenDialog}
      classes={{ paper: classes.Paper }}
    >
      <FormDialogTitle title='Блюдо' />
      <DialogContent classes={{ root: classes.Content }}>
        <TextField
          autoFocus
          required
          id='title'
          label='Название'
          margin='normal'
          variant='outlined'
          fullWidth
          value={dish.title || ''}
          onChange={updateDish('title')}
          error={!!error.title}
          helperText={error.title || ''}
          FormHelperTextProps={{ classes: { root: classes.ErrorHelperText } }}
        />
        <Grid container spacing={2} alignItems='center' wrap='nowrap'>
          <Grid item xs={8}>
            <DishCategorySelect
              category={dish.category || ''}
              setCategory={updateDish('category')}
              error={!!error.category}
              errorText={error.category || ''}
            />
          </Grid>
          <Grid item xs={4}>
            <FormControlLabel
              control={
                <Switch
                  color='primary'
                  checked={dish?.isComplex}
                  onChange={handleSwitch('isComplex')}
                />
              }
              label='Составное'
              labelPlacement='end'
            />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <TextField
              required
              id='weight'
              label='Вес'
              margin='normal'
              variant='outlined'
              fullWidth
              value={dish.weight || ''}
              onChange={updateDish('weight')}
              error={!!error.weight}
              helperText={error.weight || ''}
              FormHelperTextProps={{
                classes: { root: classes.ErrorHelperText },
              }}
            />
          </Grid>
          <Grid item>
            <TextField
              required
              id='price'
              label='Цена'
              margin='normal'
              variant='outlined'
              fullWidth
              type='number'
              value={dish.price || ''}
              onChange={updateDish('price')}
              error={!!error.price}
              helperText={error.price || ''}
              FormHelperTextProps={{
                classes: { root: classes.ErrorHelperText },
              }}
            />
          </Grid>
        </Grid>
        <TextField
          id='composition'
          label='Состав'
          margin='normal'
          variant='outlined'
          fullWidth
          value={dish.composition || ''}
          onChange={updateDish('composition')}
          error={!!error.composition}
          helperText={error.composition || ''}
          FormHelperTextProps={{
            classes: { root: classes.ErrorHelperText },
          }}
        />
        <TextField
          id='comment'
          label='Комментарий'
          margin='normal'
          variant='outlined'
          multiline
          rows={2}
          fullWidth
          value={dish.comment || ''}
          onChange={updateDish('comment')}
          error={!!error.comment}
          helperText={error.comment || ''}
          FormHelperTextProps={{
            classes: { root: classes.ErrorHelperText },
          }}
        />
      </DialogContent>
      <FormDialogActions
        loading={loading}
        handleSubmit={handleSubmit}
        handleClose={closeDialog}
      />
    </Dialog>
  );

  function onCompletedMutationHandler({ updateDish }) {
    if (updateDish.isSuccess) {
      closeDialog();
    } else {
      if (updateDish.errors.length) {
        setError(arrToObj(updateDish.errors));
      }
    }
  }

  function updateCache(cache, { data }) {
    if (dish._id || !data.updateDish.isSuccess) {
      return;
    }
    const items = cache.readQuery({ query: GET_DISHES });
    const dishes = items.dishes ? items.dishes.data : [];
    cache.writeQuery({
      query: GET_DISHES,
      data: {
        dishes: {
          ...items.dishes,
          data: [...dishes, ...data.updateDish.data],
        },
      },
    });
  }
}

export default DishDialog;
