import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Paper: {
    padding: theme.spacing(2, 0),
  },
  Content: {
    padding: theme.spacing(3),
  },
  ErrorHelperText: {
    position: 'absolute',
    top: '100%',
    marginTop: 0,
  },
}));
