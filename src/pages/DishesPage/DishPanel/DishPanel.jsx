import React from 'react';
import { Grid, Button, IconButton, Typography } from '@material-ui/core';
import { List, ListItem, ListItemText } from '@material-ui/core';
import { TextField, InputAdornment } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CheckIcon from '@material-ui/icons/Check';

import { useDishPageContext } from '../dishPageContext';
import { useQuery } from '@apollo/react-hooks';
import { GET_CATEGORIES } from '../../../graphql/query/categories';

import useStyles from './styles';

function DishPanel() {
  const classes = useStyles();
  const {
    state,
    openDialog,
    setFilter,
    setFilterValue,
    clearFilter,
  } = useDishPageContext();

  const { data } = useQuery(GET_CATEGORIES);
  const categories = data?.categories?.data || [];

  const { categoryFilter, searchFilter } = state.filter;

  const handleSelect = (_id) => () => {
    setFilterValue('categoryFilter')(_id);
  };

  return (
    <Grid container direction='column' classes={{ root: classes.GridRoot }}>
      <Grid item>{getPanelTitle()}</Grid>
      <Grid item style={{ minWidth: '160px' }}>
        {getCategoryList()}
      </Grid>
      <Grid item style={{ maxWidth: '130px' }}>
        {getSearchInput()}
      </Grid>
      <Grid item className={classes.GridClearFilter}>
        {(categoryFilter || searchFilter) && (
          <Button
            onClick={clearFilter}
            classes={{ root: classes.ButtonClearFilter }}
          >
            сброс
          </Button>
        )}
      </Grid>

      <Grid item>{getAddButton()}</Grid>
    </Grid>
  );

  function getSearchInput() {
    return (
      <TextField
        value={searchFilter}
        onChange={setFilter('searchFilter')}
        variant='outlined'
        classes={{ root: classes.SearchInputRoot }}
        InputProps={{
          startAdornment: getSearchIcon(),
          classes: { root: classes.SearchInput },
        }}
      />
    );
  }

  function getSearchIcon() {
    return (
      <InputAdornment position='start'>
        <SearchIcon fontSize='small' />
      </InputAdornment>
    );
  }

  function getCategoryList() {
    const rootDishCategory = categories.find((c) => c.title === 'Блюда');
    return (
      <List dense style={{ marginBottom: 16 }}>
        {categories
          .filter((c) => c.parent === rootDishCategory._id)
          .map((c, i) => {
            const isSelected = c._id === categoryFilter;
            return (
              <ListItem
                key={i}
                button
                selected={isSelected}
                onClick={handleSelect(c._id)}
                classes={{
                  root: classes.ListRoot,
                  selected: classes.ListSelected,
                }}
              >
                <ListItemText primary={c.title} />
                {isSelected && <CheckIcon />}
              </ListItem>
            );
          })}
      </List>
    );
  }

  function getPanelTitle() {
    return (
      <Typography variant='h4' align='right' classes={{ root: classes.Title }}>
        Блюда
      </Typography>
    );
  }

  function getAddButton() {
    return (
      <IconButton
        onClick={openDialog}
        classes={{ root: classes.IconButtonAdd }}
      >
        <AddCircleOutlineIcon classes={{ root: classes.IconAdd }} />
      </IconButton>
    );
  }
}

export default DishPanel;
