import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  GridRoot: {
    minWidth: '200px',
    background: theme.palette.primary.main,
    alignItems: 'center',
  },
  Title: {
    color: theme.palette.primary.contrastText,
    textTransform: 'uppercase',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    borderBottom: '1px solid',
  },
  ListRoot: {
    color: theme.palette.primary.contrastText,
    '&$ListSelected, &$ListSelected:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.08)',
    },
  },
  ListSelected: {},
  IconButtonAdd: {
    marginBottom: theme.spacing(5.25),
  },
  IconAdd: {
    color: theme.palette.primary.contrastText,
    fontSize: '61px',
  },
  GridClearFilter: {
    paddingTop: theme.spacing(3),
    marginBottom: 'auto',
  },
  ButtonClearFilter: {
    color: theme.palette.primary.contrastText,
  },
  SearchInput: {
    color: theme.palette.primary.contrastText,
  },
  SearchInputRoot: {
    '& label.Mui-focused': {
      color: theme.palette.primary.contrastText,
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: theme.palette.primary.contrastText,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: theme.palette.primary.contrastText,
      },
      '&:hover fieldset': {
        borderColor: theme.palette.primary.contrastText,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary.contrastText,
      },
    },
  },
}));
