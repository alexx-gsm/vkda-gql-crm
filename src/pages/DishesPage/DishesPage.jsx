import React from 'react';
import { Grid } from '@material-ui/core';
import DishContent from './DishContent';
import DishDialog from './DishDialog';
import DishPanel from './DishPanel/DishPanel';

import { DishPageProvider } from './dishPageContext';

function DishesPage() {
  return (
    <DishPageProvider>
      <Grid container wrap='nowrap' style={{ height: '100vh' }}>
        <Grid item container style={{ maxWidth: '200px' }}>
          <DishPanel />
        </Grid>
        <Grid item xs>
          <DishContent />
        </Grid>
      </Grid>
      <DishDialog />
    </DishPageProvider>
  );
}

export default DishesPage;
