import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';

const MODULE_NAME = `${APP_NAME}/DISH`;

// constants
const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const FILTER_SET = `${MODULE_NAME}/FILTER_SET`;
const FILTER_CLEAR = `${MODULE_NAME}/FILTER_CLEAR`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;
// context
const DishPageContext = React.createContext();
// initial
const emptyDish = {
  title: '',
  category: '',
  weight: '',
  price: '',
  composition: '',
  isComplex: false,
  comment: '',
};
const emptyFilter = {
  categoryFilter: '',
  searchFilter: '',
};
// immutable initial state Record
const ReducerRecord = Record({
  isOpenDialog: false,
  dish: emptyDish,
  filter: emptyFilter,
});

/**
 * reducer
 */
function dishPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state.set('isOpenDialog', false).set('dish', emptyDish);
    }
    case UPDATE_RECORD: {
      return state.setIn(['dish', payload.key], payload.value);
    }
    case FILTER_SET: {
      return state.setIn(['filter', payload.key], payload.value);
    }
    case FILTER_CLEAR: {
      return state.set('filter', emptyFilter);
    }
    case SET_ITEM: {
      return state.set('dish', payload.dish).set('isOpenDialog', true);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 * provider
 */
function DishPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    dishPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <DishPageContext.Provider value={value} {...props} />;
}

/**
 * context
 */
function useDishPageContext() {
  const context = React.useContext(DishPageContext);
  if (!context) {
    throw new Error(
      `useDishPageContext must be used within a DishPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateDish = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const setValue = (key) => (value) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value } });
  const setFilter = (key) => (e) =>
    dispatch({ type: FILTER_SET, payload: { key, value: e.target.value } });
  const setFilterValue = (key) => (value) =>
    dispatch({ type: FILTER_SET, payload: { key, value: value } });
  const clearFilter = () => dispatch({ type: FILTER_CLEAR });
  const storeDish = (dish) => dispatch({ type: SET_ITEM, payload: { dish } });

  return {
    state,
    openDialog,
    closeDialog,
    updateDish,
    setValue,
    setFilter,
    setFilterValue,
    clearFilter,
    storeDish,
  };
}

export { DishPageProvider, useDishPageContext };
