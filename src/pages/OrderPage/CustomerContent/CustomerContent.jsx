import React from 'react';
import {
  Table,
  TableContainer,
  TableHead,
  TableBody,
  Grid,
} from '@material-ui/core';
import { TableRow, TableCell } from '@material-ui/core';
import { Paper, Typography } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import {
  StyledTableCell,
  StyledTableRow,
} from '../../../components/table/StyledTable.jsx';

import { useQuery } from '@apollo/react-hooks';
import { useCustomerPageContext } from '../customerPageContext';
import { GET_CUSTOMERS } from '../../../graphql/query';

const ITEMS_PER_PAGE = 10;

const getTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell>Название</StyledTableCell>
      </TableRow>
    </TableHead>
  );
};

function CustomerContent() {
  const { data, loading } = useQuery(GET_CUSTOMERS);
  const {
    state: { filter },
    setCustomer,
  } = useCustomerPageContext();

  const { searchFilter } = filter;

  //** PAGING STATE */
  const [page, setPage] = React.useState(1);

  const handleChange = (event, value) => {
    setPage(value);
  };

  React.useEffect(() => {
    setPage(1);
  }, [searchFilter]);

  //** FILTER STAGE */
  const [customers, setCustomers] = React.useState([]);

  // TODO: add useCallback: getCustomer
  React.useEffect(() => {
    if (!data?.customers?.data) {
      return;
    }
    if (!searchFilter) {
      setCustomers(data.customers.data);
    }

    setCustomers(getCustomer(data.customers.data));
  }, [data, searchFilter, page]);

  if (loading) {
    return (
      <Typography variant='h6' align='center'>
        loading...
      </Typography>
    );
  }

  const handleClick = (customer) => () => {
    setCustomer({
      ...customer,
      courier: customer.courier?._id || '',
    });
  };

  const count = Math.ceil(customers.length / ITEMS_PER_PAGE);

  return (
    <>
      <TableContainer component={Paper}>
        <Table size='small'>
          {getTableHead()}
          <TableBody>
            {customers
              .slice(ITEMS_PER_PAGE * (page - 1), ITEMS_PER_PAGE * page)
              .map((customer) => {
                return (
                  <StyledTableRow
                    key={customer._id}
                    onClick={handleClick(customer)}
                  >
                    <TableCell style={{ width: '100%' }}>
                      <Grid container spacing={1} alignItems='center'>
                        <Grid item>
                          <Typography variant='h6'>
                            {customer?.company?.title || 'ф.л.'}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant='caption'>
                            {`${customer?.company?.address || ''} ${
                              customer?.address
                            }`}
                          </Typography>
                        </Grid>
                      </Grid>
                      <Grid container spacing={1} alignItems='center'>
                        <Grid item>
                          <Typography variant='subtitle1'>
                            {customer?.name}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography variant='subtitle2'>
                            {customer?.phone}
                          </Typography>
                        </Grid>
                      </Grid>
                    </TableCell>
                  </StyledTableRow>
                );
              })}
          </TableBody>
        </Table>
        {customers.length === 0 && (
          <Typography variant='h6' align='center'>
            Нет данных
          </Typography>
        )}
      </TableContainer>
      {count > 1 && (
        <Grid container justify='center' style={{ marginTop: '24px' }}>
          <Pagination
            count={count}
            page={page}
            onChange={handleChange}
            variant='outlined'
            color='secondary'
          />
        </Grid>
      )}
    </>
  );

  function getCustomer(_customers) {
    return getFilteredCustomers(_customers).sort(sortByCompany);
  }

  function getFilteredCustomers(_customers) {
    return searchFilter
      ? _customers.filter((customer) => {
          return [
            customer.company.title,
            customer.company.address,
            customer.name,
            customer.address,
            customer.phone,
          ].some(
            (field) =>
              field &&
              field.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1,
          );
        })
      : _customers;
  }

  function sortByCompany(a, b) {
    if (a.company.title && b.company.title) {
      return a.company.title.toLowerCase() > b.company.title.toLowerCase()
        ? 1
        : -1;
    }

    if (!a.company.title && !b.company.title) {
      return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
    }

    if (!a.company.title) {
      return 1;
    }

    if (!b.company.title) {
      return -1;
    }
  }
}

export default CustomerContent;
