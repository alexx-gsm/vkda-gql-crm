import React from 'react'
import { Observer } from 'mobx-react-lite'
import { Grid, Button, Typography, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import SearchInput from '../../../components/common/SearchInput/SearchInput'

import { useOrderPageStore } from '../OrderPage'

import useStyles from './styles'

function OrderPanel() {
  const classes = useStyles()

  const orderStore = useOrderPageStore()
  const { searchFilter } = orderStore.filters

  return (
    <Observer>
      {() => (
        <Grid
          container
          alignItems='flex-end'
          spacing={2}
          classes={{ root: classes.GridRoot }}
        >
          <Grid item>
            <Button
              variant='contained'
              color='secondary'
              onClick={orderStore.openDialog}
            >
              Добавить
            </Button>
          </Grid>
          <Grid item>
            <SearchInput
              value={searchFilter}
              handleChange={orderStore.setFilter('searchFilter')}
            />
          </Grid>
          <Grid item>
            <IconButton
              size='small'
              disabled={!searchFilter}
              onClick={orderStore.clearFilter}
            >
              <CloseIcon fontSize='small' />
            </IconButton>
          </Grid>
          <Grid item xs>
            <Typography variant='h4' align='right'>
              Заказы
            </Typography>
          </Grid>
        </Grid>
      )}
    </Observer>
  )
}

export default OrderPanel
