import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';
import moment from 'moment';

const MODULE_NAME = `${APP_NAME}/ORDER`;

const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;
const SET_VALUE = `${MODULE_NAME}/SET_VALUE`;
const FILTER_SET = `${MODULE_NAME}/FILTER_SET`;
const FILTER_CLEAR = `${MODULE_NAME}/FILTER_CLEAR`;

const OrderPageContext = React.createContext();
/**
 ** State initial
 */
const emptyOrder = {
  number: '',
  date: moment(),
  order: {},
  customer: null,
  paymentType: 'cash',
  discount: '0',
  address: '',
  courier: '',
  comment: '',
  dishes: [],
};
const emptyFilter = {
  searchFilter: '',
};
const ReducerRecord = Record({
  isOpenDialog: false,
  order: emptyOrder,
  filter: emptyFilter,
});
/**
 ** reducer
 */
function orderPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state.set('isOpenDialog', false).set('order', emptyOrder);
    }
    case UPDATE_RECORD: {
      return state.setIn(['order', payload.key], payload.value);
    }
    case SET_ITEM: {
      return state.set('order', payload.order).set('isOpenDialog', true);
    }
    case SET_VALUE: {
      return state.setIn(['order', payload.key], payload.value);
    }
    case FILTER_SET: {
      return state.setIn(['filter', payload.key], payload.value);
    }
    case FILTER_CLEAR: {
      return state.set('filter', emptyFilter);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 ** provider
 */
function OrderPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    orderPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <OrderPageContext.Provider value={value} {...props} />;
}

/**
 ** context
 */
function useOrderPageContext() {
  const context = React.useContext(OrderPageContext);
  if (!context) {
    throw new Error(
      `useOrderPageContext must be used within a OrderPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateValue = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const setOrder = (order) => dispatch({ type: SET_ITEM, payload: { order } });
  const setValue = (key) => (value) =>
    dispatch({ type: SET_VALUE, payload: { key, value } });
  const setFilter = (key) => (e) =>
    dispatch({ type: FILTER_SET, payload: { key, value: e.target.value } });
  const clearFilter = () => dispatch({ type: FILTER_CLEAR });

  return {
    state,
    openDialog,
    closeDialog,
    updateValue,
    setOrder,
    setValue,
    setFilter,
    clearFilter,
  };
}

export { OrderPageProvider, useOrderPageContext };
