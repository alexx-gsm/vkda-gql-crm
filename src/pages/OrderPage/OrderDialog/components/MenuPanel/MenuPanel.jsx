import React from 'react';
import { autorun } from 'mobx';
import { Observer } from 'mobx-react-lite';
import { Box, Grid, Typography } from '@material-ui/core';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import { Card, CardActionArea, CardContent } from '@material-ui/core';
import TabPanel from '../../../../../components/form/TabPanel';
import { Icon } from 'react-icons-kit';
import { u1F35C } from 'react-icons-kit/noto_emoji_regular/u1F35C';
// import { u1F35A } from 'react-icons-kit/noto_emoji_regular/u1F35A';
// import { u1F357 } from 'react-icons-kit/noto_emoji_regular/u1F357';

import { useOrderPageStore } from '../../../OrderPage';

import { GET_MENU_BY_DATE, GET_CATEGORIES } from '../../../../../graphql/query';
import { useQuery } from '@apollo/react-hooks';
import formatter from '../../../../../utils/formatter';
import useStyles from './styles';
import COLORS from './colors';
import moment from 'moment';

function MenuPanel() {
  const classes = useStyles();
  const orderStore = useOrderPageStore();
  //! TABS
  const [tabIndex, setTabIndex] = React.useState(0);

  //! CATEGORIES
  const { data: dataCategories } = useQuery(GET_CATEGORIES);
  const categories = dataCategories?.categories?.data || [];

  //! MENU
  const { data: dataMenu, refetch: refechMenu } = useQuery(GET_MENU_BY_DATE, {
    variables: { date: moment(orderStore.order.date).startOf('day') },
  });
  const menu = dataMenu?.menuByDate?.data[0] || {};

  React.useEffect(() => {
    autorun(() => {
      refechMenu({ date: orderStore.order.date });
      console.log('date', orderStore.order.date);
      console.log('moment date', moment(orderStore.order.date));
      orderStore.setValue('dishes')([]);
      setTabIndex(0);
    });
  }, []);

  const handleChangeTab = (event, newValue) => {
    setTabIndex(newValue);
  };

  const handleAddDish = (dish, count = 1, complexDish = null) => () => {
    const dishIndex = orderStore.order.dishes.findIndex(
      (row) => row.dish._id === dish._id,
    );
    if (dishIndex === -1) {
      orderStore.setValue('dishes')([
        ...orderStore.order.dishes,
        {
          dish,
          count,
          complexDish,
        },
      ]);
    } else {
      //todo: refactor with complexDishes
      orderStore.setValue('dishes')([
        ...orderStore.order.dishes.slice(0, dishIndex),
        {
          dish,
          count: count + orderStore.order.dishes[dishIndex].count,
          complexDish: null,
        },
        ...orderStore.order.dishes.slice(dishIndex + 1),
      ]);
    }
  };

  const filteredMenu = getCategoriedDishes();

  return (
    <Observer>
      {() => (
        <Box classes={{ root: classes.BoxRoot }}>
          {filteredMenu && (
            <>
              {filteredMenu.map(({ dishes }, index) => (
                <TabPanel
                  key={index}
                  value={tabIndex}
                  index={index}
                  style={{ minWidth: '220px' }}
                >
                  {dishes.map((d) => renderDish(d, COLORS[index].color))}
                </TabPanel>
              ))}
              <AppBar position='static' classes={{ root: classes.AppBar }}>
                <Tabs
                  value={tabIndex}
                  onChange={handleChangeTab}
                  orientation='vertical'
                  indicatorColor='primary'
                  classes={{
                    root: classes.Tabs,
                    indicator: classes.TabsIndicator,
                  }}
                >
                  {filteredMenu.map(({ title }) => (
                    <Tab
                      key={title}
                      icon={<Icon icon={u1F35C} size={32} />}
                      label={title}
                      classes={{
                        root: classes.Tab,
                        selected: classes.TabSelected,
                      }}
                    />
                  ))}
                </Tabs>
              </AppBar>
            </>
          )}
        </Box>
      )}
    </Observer>
  );

  function renderDish(dish, color) {
    return (
      <Card
        key={dish._id}
        classes={{ root: classes.CardDish }}
        style={{ background: color }}
        onClick={handleAddDish(dish)}
      >
        <CardActionArea>
          <CardContent classes={{ root: classes.CardContent }}>
            <Grid container direction='column'>
              <Grid item classes={{ root: classes.GridCardTitle }}>
                <Typography variant='h6' noWrap>
                  {dish.title}
                </Typography>
              </Grid>
              <Grid
                item
                container
                justify='space-between'
                alignItems='flex-end'
              >
                <Grid item>остатки: 5</Grid>
                <Grid item>
                  <Typography variant='h5' noWrap>
                    {formatter.format(dish.price)}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }

  function getCategoriedDishes() {
    if (menu?.dishes?.length && categories.length) {
      const rootCategoryId = categories.find((c) => c.title === 'Блюда')?._id;

      return categories
        .filter((c) => c.parent === rootCategoryId)
        .sort((a, b) => a.sorting - b.sorting)
        .reduce((acc, c) => {
          const catDishes = menu.dishes.filter((d) => d.category._id === c._id);
          return catDishes.length
            ? [...acc, { title: c.title, dishes: catDishes }]
            : acc;
        }, []);
    }
  }
}

export default MenuPanel;
