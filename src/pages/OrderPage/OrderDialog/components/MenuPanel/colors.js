import {
  red,
  purple,
  teal,
  green,
  deepOrange,
  brown,
  blueGrey,
  lime,
} from '@material-ui/core/colors';

const COLORS = [
  { key: 'red', color: red[50] },
  { key: 'purple', color: purple[50] },
  { key: 'teal', color: teal[50] },
  { key: 'green', color: green[50] },
  { key: 'deepOrange', color: deepOrange[50] },
  { key: 'brown', color: brown[50] },
  { key: 'blueGrey', color: blueGrey[50] },
  { key: 'lime', color: lime[50] },
];

export default COLORS;
