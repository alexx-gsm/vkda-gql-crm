import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  BoxRoot: {
    padding: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    margin: 0,
    height: '100%',
  },
  AppBar: {
    width: '120px',
    height: '100%',
  },
  Tabs: {},
  TabsIndicator: {
    width: '5px',
  },
  Tab: {
    minWidth: '40px',
  },
  TabSelected: {
    background: theme.palette.primary.contrastText,
    color: theme.palette.primary.main,
  },
  CardDish: {
    marginBottom: theme.spacing(1),
    maxWidth: '200px',
    width: '100%',
    boxShadow: 'none',
    border: '1px solid',
    background: 'antiquewhite',
  },
  CardContent: {
    padding: theme.spacing(1),
  },
  GridCardTitle: {
    overflow: 'hidden',
    width: '100%',
  },
}));
