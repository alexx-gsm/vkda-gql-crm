import React from 'react'
import { Paper } from '@material-ui/core'
import CustomerSelect from '../../../../../components/form/CustomerSelect'

import { useOrderPageContext } from '../../../orderPageContext'

import useStyles from './styles'

function CustomerPanel() {
  const classes = useStyles()

  const {
    state: { order },
    setValue,
  } = useOrderPageContext()

  return (
    <Paper variant='outlined' square classes={{ root: classes.PaperCustomer }}>
      <CustomerSelect
        customer={order.customer}
        setCustomer={setValue('customer')}
      />
    </Paper>
  )
}

export default CustomerPanel
