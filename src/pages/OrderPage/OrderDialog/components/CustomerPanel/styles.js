import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  PaperCustomer: {
    width: '100%',
    padding: theme.spacing(1, 2),
    border: 0,
    boxShadow: 0,
  },
}));
