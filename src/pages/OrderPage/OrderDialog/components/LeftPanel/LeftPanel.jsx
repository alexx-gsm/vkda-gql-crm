import React from 'react'
import { Observer } from 'mobx-react-lite'
import { Grid, TextField, Typography, Badge, Divider } from '@material-ui/core'
import { InputAdornment } from '@material-ui/core'
import { Paper, IconButton, Button } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import FormDatePicker from '../../../../../components/form/FormDatePicker'
import CourierSelect from '../../../../../components/form/CourierSelect'
import PaymentTypeSelect from '../../../../../components/form/PaymentTypeSelect'

import { useOrderPageStore } from '../../../OrderPage'

import moment from 'moment'

import useStyles from './styles'

function LeftPanel() {
  const classes = useStyles()
  const orderStore = useOrderPageStore()

  const handleClickTodayButton = () => orderStore.setValue('date')(moment())

  const handleClickTomorrowButton = () =>
    orderStore.setValue('date')(moment().add(1, 'days'))

  return (
    <Observer>
      {() => (
        <Paper classes={{ root: classes.PaperPanel }}>
          <Grid
            container
            direction='column'
            style={{ height: '100%', minWidth: '175px' }}
          >
            {getGridTitle()}
            {getGridOrderNumber()}
            {getOrderDateBlock()}
            <Divider classes={{ root: classes.Divider }} />
            {getCourierSelect()}
            {getPaymentTypeSelect()}
            {getDiscountInput()}
            {getActions()}
          </Grid>
        </Paper>
      )}
    </Observer>
  )

  function getActions() {
    return (
      <Button
        variant='contained'
        color='primary'
        size='large'
        classes={{ root: classes.ButtonSave }}
      >
        Сохранить
      </Button>
    )
  }

  function getDiscountInput() {
    return (
      <TextField
        label='Скидка'
        margin='dense'
        fullWidth
        type='number'
        InputProps={{
          endAdornment: <InputAdornment position='end'>%</InputAdornment>,
        }}
        value={+orderStore.order.discount}
        onChange={orderStore.setValue('discount')}
      />
    )
  }

  function getCourierSelect() {
    return (
      <CourierSelect
        courier={orderStore.order.courier}
        setCourier={orderStore.setValue('courier')}
      />
    )
  }

  function getPaymentTypeSelect() {
    return (
      <PaymentTypeSelect
        paymentType={orderStore.order.paymentType}
        setPaymentType={orderStore.setValue('paymentType')}
      />
    )
  }

  function getOrderDateBlock() {
    return (
      <Grid
        item
        container
        direction='column'
        spacing={1}
        classes={{ root: classes.GridSelectDate }}
      >
        <Grid item>
          <FormDatePicker
            date={orderStore.order.date}
            handleDateChange={orderStore.setValue('date')}
          />
        </Grid>
        <Grid item container justify='space-between'>
          <Button
            disabled={moment(orderStore.order.date).isSame(moment(), 'day')}
            onClick={handleClickTodayButton}
          >
            Сегодня
          </Button>
          <Button
            disabled={moment(orderStore.order.date).isSame(
              moment().add(1, 'days'),
              'day',
            )}
            onClick={handleClickTomorrowButton}
          >
            Завтра
          </Button>
        </Grid>
      </Grid>
    )
  }

  function getGridOrderNumber() {
    return (
      <Grid item container justify='flex-end'>
        <Typography
          variant='caption'
          align='right'
          classes={{ root: classes.TypoNumber }}
        >{`№ ${'25-125'}`}</Typography>
      </Grid>
    )
  }

  function getGridTitle() {
    return (
      <Grid
        item
        container
        alignItems='center'
        justify='space-between'
        classes={{ root: classes.GridPanelTitle }}
      >
        <Grid item>
          <IconButton
            edge='start'
            color='inherit'
            onClick={orderStore.closeDialog}
            aria-label='close'
          >
            <CloseIcon />
          </IconButton>
        </Grid>
        <Grid item>
          <Badge color='error' badgeContent=' ' variant='dot'>
            <Typography variant='h4' classes={{ root: classes.PanelTitle }}>
              Заказ
            </Typography>
          </Badge>
        </Grid>
      </Grid>
    )
  }
}

export default LeftPanel
