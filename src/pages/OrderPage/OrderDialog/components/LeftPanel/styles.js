import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  PaperPanel: {
    maxWidth: '210px',
    padding: theme.spacing(1, 2),
    flex: 1,
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    borderRadius: 0,
  },
  PanelTitle: {
    textTransform: 'uppercase',
  },
  TypoNumber: {
    fontWeight: theme.typography.fontWeightLight,
    borderBottom: '1px solid',
    fontSize: '1.2rem',
    lineHeight: 1.2,
  },
  GridSelectDate: {
    marginTop: theme.spacing(3),
  },
  ButtonSelectDate: {
    color: theme.palette.secondary.contrastText,
  },
  Divider: {
    margin: theme.spacing(3, 1),
  },
  ButtonSave: {
    marginTop: 'auto',
    marginBottom: theme.spacing(2),
    color: theme.palette.secondary.contrastText,
  },
}));
