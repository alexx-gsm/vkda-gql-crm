import React from 'react'
import { Observer } from 'mobx-react-lite'
import { Box, Paper, Grid, Typography } from '@material-ui/core'

import { useOrderPageStore } from '../../../OrderPage'

import formatter from '../../../../../utils/formatter'
import useStyles from './styles'

function DishesList() {
  const classes = useStyles()
  const orderStore = useOrderPageStore()

  return (
    <Observer>
      {() =>
        !!orderStore.order.dishes.length && (
          <Box px={2}>
            <Paper
              square
              variant='outlined'
              classes={{ root: classes.PaperWrap }}
            >
              {orderStore.order.dishes.map(({ dish, count }, i) => (
                <Grid
                  key={i}
                  container
                  alignItems='baseline'
                  classes={{ root: classes.GridDishRow }}
                >
                  <Grid item style={{ width: 25 }}>
                    <Typography variant='caption'>{i + 1}.</Typography>
                  </Grid>
                  <Grid item xs>
                    <Typography variant='h6'>{dish.title}</Typography>
                  </Grid>
                  <Grid item>
                    <Typography
                      variant='h4'
                      classes={{ root: classes.TypoCount }}
                    >
                      {count}
                      <span>шт.</span>
                    </Typography>
                  </Grid>
                  <Grid item xs={2}>
                    <Typography
                      variant='h4'
                      align='right'
                      classes={{ root: classes.TypoPrice }}
                    >
                      {formatter.format(dish.price * count)}
                    </Typography>
                  </Grid>
                </Grid>
              ))}
            </Paper>
            <Grid
              container
              justify='flex-end'
              alignItems='baseline'
              classes={{ root: classes.GridTotalPrice }}
            >
              <Grid item>
                <Typography variant='h6'>ИТОГО:</Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography
                  variant='h3'
                  align='right'
                  classes={{ root: classes.TypoTotalPrice }}
                >
                  {formatter.format(orderStore.getTotalPrice)}
                </Typography>
              </Grid>
            </Grid>
          </Box>
        )
      }
    </Observer>
  )
}

export default DishesList
