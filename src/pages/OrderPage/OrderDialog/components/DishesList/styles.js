import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(theme => ({
  PaperWrap: {},
  GridDishRow: {
    padding: theme.spacing(0, 2),
    '&:nth-child(even)': {
      background: theme.palette.grey[100],
    },
  },
  TypoPrice: {
    fontWeight: theme.typography.fontWeightMedium,
  },
  TypoCount: {
    fontWeight: theme.typography.fontWeightLight,
    '& span': {
      fontSize: '1rem',
    },
  },
  GridTotalPrice: {},
  TypoTotalPrice: {
    color: theme.palette.primary.dark,
    fontWeight: theme.palette.fontWeightMedium,
  },
}))
