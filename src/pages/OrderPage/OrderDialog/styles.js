import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  DialogPaper: {
    display: 'flex',
    flexDirection: 'row',
  },
  Content: {
    padding: 0,
    display: 'flex',
  },
}));
