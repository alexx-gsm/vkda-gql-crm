import React from 'react'
import { Observer } from 'mobx-react-lite'
import { Grid, Dialog, DialogContent, Slide } from '@material-ui/core'
import LeftPanel from './components/LeftPanel'
import DishesList from './components/DishesList'
import CustomerPanel from './components/CustomerPanel'
import OrderDialogMenu from './components/MenuPanel'

import { useOrderPageStore } from '../OrderPage'

import useStyles from './styles'

function OrderDialog() {
  const classes = useStyles()
  const orderStore = useOrderPageStore()

  return (
    <Observer>
      {() => (
        <Dialog
          fullScreen
          TransitionComponent={Slide}
          TransitionProps={{ direction: 'up' }}
          onClose={orderStore.closeDialog}
          open={orderStore.isOpenDialog}
          classes={{ root: classes.DialogRoot, paper: classes.DialogPaper }}
        >
          <LeftPanel />
          <DialogContent classes={{ root: classes.Content }}>
            <Grid container>
              <Grid item xs>
                <CustomerPanel />
                <DishesList />
              </Grid>
              <Grid item>
                <OrderDialogMenu />
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
      )}
    </Observer>
  )
}

export default OrderDialog
