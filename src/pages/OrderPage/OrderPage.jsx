import React from 'react';
import { Container } from '@material-ui/core';
import OrderPanel from './OrderPanel';
// import OrderContent from './OrderContent';
import OrderDialog from './OrderDialog';

import { OrderPageProvider } from './orderPageContext';
import OrderStore from '../../stores/order-store';

const StoreContext = React.createContext();
const StoreProvider = StoreContext.Provider;

const orderPageStore = new OrderStore();

function OrderPage() {
  return (
    <Container maxWidth={false} disableGutters>
      <StoreProvider value={orderPageStore}>
        <OrderPageProvider>
          <OrderPanel />
          {/* <OrderContent /> */}
          <OrderDialog />
        </OrderPageProvider>
      </StoreProvider>
    </Container>
  );
}

export default OrderPage;

export const useOrderPageStore = () => React.useContext(StoreContext);
