import React from 'react';
import { Container } from '@material-ui/core';
import CategoryPanel from './CategoryPanel';
import CategoryDialog from './CategoryDialog';

import { CategoryPageProvider } from './categoryPageContext';
import CategoryContent from './CategoryContent/CategoryContent';

function CategoryPage() {
  return (
    <Container maxWidth='md' disableGutters>
      <CategoryPageProvider>
        <CategoryPanel />
        <CategoryContent />
        <CategoryDialog />
      </CategoryPageProvider>
    </Container>
  );
}

export default CategoryPage;
