import React from 'react';
import { Grid, Button, Typography } from '@material-ui/core';
// context
import { useCategoryPageContext } from '../categoryPageContext';
// styles
import useStyles from './styles';

function CategoryPanel() {
  const classes = useStyles();
  const { openDialog } = useCategoryPageContext();

  return (
    <Grid
      container
      alignItems='flex-end'
      spacing={2}
      classes={{ root: classes.GridRoot }}
    >
      <Grid item>
        <Button variant='contained' color='secondary' onClick={openDialog}>
          Добавить
        </Button>
      </Grid>
      <Grid item xs>
        <Typography variant='h4' align='right'>
          Категории
        </Typography>
      </Grid>
    </Grid>
  );
}

export default CategoryPanel;
