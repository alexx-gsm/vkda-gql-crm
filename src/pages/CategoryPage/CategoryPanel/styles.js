import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
  GridRoot: {
    marginBottom: theme.spacing(2),
  },
}))
