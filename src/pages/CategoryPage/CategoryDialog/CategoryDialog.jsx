import React from 'react';
import { TextField, Grid } from '@material-ui/core';
import { Dialog, DialogContent } from '@material-ui/core';
import DishCategorySelect from '../../../components/form/DishCategorySelect';
import FormDialogActions from '../../../components/form/FormDialogActions';
import FormDialogTitle from '../../../components/form/FormDialogTitle';

import { useCategoryPageContext } from '../categoryPageContext';
import { useMutation } from '@apollo/react-hooks';
import { EDIT_CATEGORY } from '../../../graphql/mutation';
import { GET_CATEGORIES } from '../../../graphql/query';

import { arrToObj } from '../../../utils/arrToObj';
import useStyles from './styles';

function CategoryDialog() {
  const classes = useStyles();
  const [error, setError] = React.useState({});
  const { state, closeDialog, updateCategory } = useCategoryPageContext();
  const { isOpenDialog, category } = state;
  const [edit, { loading }] = useMutation(EDIT_CATEGORY, {
    onCompleted: onCompletedMutationHandler,
    update: updateCache,
    onError: (error) => console.log('edit category error:', error),
  });

  React.useEffect(() => {
    if (Object.keys(error).length && !state.isOpenDialog) {
      setError({});
    }
  }, [error, setError, state.isOpenDialog]);

  const handleSubmit = (e) => {
    e.preventDefault();
    edit({ variables: category });
  };

  return (
    <Dialog
      fullWidth
      maxWidth='sm'
      open={isOpenDialog}
      classes={{ paper: classes.Paper }}
    >
      <form onSubmit={handleSubmit}>
        <FormDialogTitle title='Категория' />
        <DialogContent classes={{ root: classes.Content }}>
          <TextField
            autoFocus
            required
            id='title'
            label='Название'
            margin='normal'
            variant='outlined'
            fullWidth
            value={category.title}
            onChange={updateCategory('title')}
            error={!!error.title}
            helperText={error.title || ''}
            FormHelperTextProps={{ classes: { root: classes.ErrorHelperText } }}
          />
          <Grid container alignItems='center' spacing={2}>
            <Grid item xs={8}>
              <DishCategorySelect
                category={category.parent || ''}
                setCategory={updateCategory('parent')}
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id='sorting'
                label='Сортировка'
                margin='normal'
                variant='outlined'
                type='number'
                min={0}
                step={1}
                value={category.sorting || 0}
                onChange={updateCategory('sorting')}
              />
            </Grid>
          </Grid>
          <TextField
            id='description'
            label='Комментарий'
            margin='normal'
            variant='outlined'
            multiline
            rows={3}
            fullWidth
            value={category.description || ''}
            onChange={updateCategory('description')}
          />
        </DialogContent>
        <FormDialogActions
          loading={loading}
          handleSubmit={handleSubmit}
          handleClose={closeDialog}
        />
      </form>
    </Dialog>
  );

  function onCompletedMutationHandler({ editCategory }) {
    if (editCategory.isSuccess) {
      closeDialog();
    } else {
      if (editCategory.errors.length) {
        setError(arrToObj(editCategory.errors));
      }
    }
  }

  function updateCache(cache, { data }) {
    if (category._id) {
      return;
    }
    const items = cache.readQuery({ query: GET_CATEGORIES });
    cache.writeQuery({
      query: GET_CATEGORIES,
      data: {
        categories: {
          ...items.categories,
          data: [...items.categories.data, ...data.editCategory.data],
        },
      },
    });
  }
}

export default CategoryDialog;
