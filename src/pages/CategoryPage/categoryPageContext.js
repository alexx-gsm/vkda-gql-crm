import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';

const MODULE_NAME = `${APP_NAME}/CATEGORY`;
// constants
const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_ITEM = `${MODULE_NAME}/UPDATE_ITEM`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;
// context
const CategoryPageContext = React.createContext();
// initial
const emptyEntity = {
  title: '',
  parent: '',
  sorting: '',
  description: '',
};
// immutable state Record
const ReducerRecord = Record({
  isOpenDialog: false,
  category: emptyEntity,
});

/**
 * reducer
 */
function categoryPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state.set('isOpenDialog', false).set('category', emptyEntity);
    }
    case UPDATE_ITEM: {
      return state.setIn(['category', payload.key], payload.value);
    }
    case SET_ITEM: {
      return state.set('category', payload.category).set('isOpenDialog', true);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 * provider
 */
function CategoryPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    categoryPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <CategoryPageContext.Provider value={value} {...props} />;
}

/**
 * context
 */
function useCategoryPageContext() {
  const context = React.useContext(CategoryPageContext);
  if (!context) {
    throw new Error(
      `useDishPageContext must be used within a CategoryPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateCategory = (key) => (e) =>
    dispatch({ type: UPDATE_ITEM, payload: { key, value: e.target.value } });
  const storeCategory = (category) =>
    dispatch({ type: SET_ITEM, payload: { category } });

  return {
    state,
    openDialog,
    closeDialog,
    updateCategory,
    storeCategory,
  };
}

export { CategoryPageProvider, useCategoryPageContext };
