import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import { TreeView, TreeItem } from '@material-ui/lab';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import EditIcon from '@material-ui/icons/Edit';

import { useQuery } from '@apollo/react-hooks';
import { useCategoryPageContext } from '../categoryPageContext';
import { GET_CATEGORIES } from '../../../graphql/query';

function CategoryContent() {
  const { data, loading } = useQuery(GET_CATEGORIES);
  const { storeCategory } = useCategoryPageContext();

  if (loading) {
    return (
      <Typography variant='h6' align='center'>
        loading...
      </Typography>
    );
  }

  if (!loading && !data.categories.data.length) {
    return (
      <Typography variant='h6' align='center'>
        нет данных
      </Typography>
    );
  }

  const handleEditIconClick = (id) => (e) => {
    e.preventDefault();
    storeCategory(getCategory(id));
  };

  const categories = getSortedCategories();

  return (
    <TreeView
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      children={getTreeItems()}
    />
  );

  function getTreeItems(parent = null) {
    return categories
      .filter((item) => item.parent === parent)
      .sort((a, b) => a.sorting - b.sorting)
      .map((item) => {
        const children = categories.filter(
          (child) => child.parent === item._id,
        );
        return (
          <StyledTreeItem
            key={item._id}
            nodeId={item._id}
            {...item}
            children={children.length ? getTreeItems(item._id) : ''}
          />
        );
      });
  }

  function StyledTreeItem({ _id, title, sorting, ...props }) {
    const hasChildren = props.children.length;

    return (
      <TreeItem
        label={
          <Grid container alignItems='center' justify='space-between'>
            <Typography variant='h6'>{title}</Typography>
            {hasChildren ? (
              getEndIcon(_id)
            ) : (
              <Typography variant='caption'>({sorting})</Typography>
            )}
          </Grid>
        }
        {...props}
        onLabelClick={hasChildren ? null : handleEditIconClick(_id)}
      />
    );
  }

  function getEndIcon(id) {
    return <EditIcon onClick={handleEditIconClick(id)} fontSize='small' />;
  }

  function getCategory(_id) {
    return categories.find((c) => c._id === _id);
  }

  function getSortedCategories() {
    return data.categories.data.sort((a, b) => a.sorting - b.sorting);
  }
}

export default CategoryContent;
