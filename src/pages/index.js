import React, { lazy, Suspense } from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import AdminLayout from '../layouts/AdminLayout';

const SignIn = lazy(() => import('./Login'));
// const Home = lazy(() => import('./Home'));
const DishesPage = lazy(() => import('./DishesPage'));
const CategoryPage = lazy(() => import('./CategoryPage'));
const MenuPage = lazy(() => import('./MenuPage'));
// const CourierPage = lazy(() => import('./CourierPage'));
// const CustomerPage = lazy(() => import('./CustomerPage'));
// const CompanyPage = lazy(() => import('./CompanyPage'));
// const OrderPage = lazy(() => import('./OrderPage'));
const Error404Page = lazy(() => import('./Error404Page'));

export default function Pages() {
  return (
    <Router>
      <Suspense fallback={<h2>Loading......</h2>}>
        <Switch>
          <Route path='/signin' exact component={SignIn} />
          <AdminLayout>
            <Switch>
              {/* <Route path='/' exact component={Home} /> */}
              <Route path='/dishes' component={DishesPage} />
              <Route path='/categories' component={CategoryPage} />
              <Route path='/menu' component={MenuPage} />
              {/* <Route path='/couriers' component={CourierPage} />
              <Route path='/customers' component={CustomerPage} />
              <Route path='/companies' component={CompanyPage} />
              <Route path='/orders' component={OrderPage} /> */}
              <Redirect to='/menu' />
              <Route component={Error404Page} />
            </Switch>
          </AdminLayout>
        </Switch>
      </Suspense>
    </Router>
  );
}
