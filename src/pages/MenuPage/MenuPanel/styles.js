import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

export default makeStyles((theme) => ({
  GridRoot: {
    minWidth: '200px',
    background: theme.palette.primary.main,
    alignItems: 'center',
  },
  Title: {
    color: theme.palette.primary.contrastText,
    textTransform: 'uppercase',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    borderBottom: '1px solid',
  },
  IconButtonAdd: {
    marginBottom: theme.spacing(5.25),
  },
  IconAdd: {
    color: theme.palette.primary.contrastText,
    fontSize: '61px',
  },
  ButtonRootWeek: {
    color: theme.palette.primary.contrastText,
    borderColor: theme.palette.primary.contrastText,
  },
  ButtonLabelWeek: {
    display: 'flex',
    flexDirection: 'column',
  },
  dayWrapper: {
    position: 'relative',
  },
  day: {
    width: 36,
    height: 36,
    fontSize: theme.typography.caption.fontSize,
    margin: '0 2px',
    color: 'inherit',
  },
  customDayHighlight: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: '2px',
    right: '2px',
    border: `1px solid ${theme.palette.secondary.main}`,
    borderRadius: '50%',
  },
  nonCurrentMonthDay: {
    color: theme.palette.text.disabled,
  },
  highlightNonCurrentMonthDay: {
    color: '#676767',
  },
  highlight: {
    background: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  firstHighlight: {
    borderTopLeftRadius: '50%',
    borderBottomLeftRadius: '50%',
  },
  endHighlight: {
    borderTopRightRadius: '50%',
    borderBottomRightRadius: '50%',
  },
  holiday: {
    color: red[900],
  },
}));
