import React from 'react';
import { Grid, Button, IconButton, Typography } from '@material-ui/core';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';

import { useMenuPageContext } from '../menuPageContext';

import moment from 'moment';
import MomentUtils from '@date-io/moment';
import clsx from 'clsx';
import useStyles from './styles';

function MenuPanel() {
  const classes = useStyles();
  const { state, openDialog, setFilter } = useMenuPageContext();
  const week = moment(state.filter.week);

  const [open, setOpen] = React.useState(false);

  const handleWeekChenge = (day) => setFilter('week')(moment(day).isoWeek());

  return (
    <Grid container direction='column' classes={{ root: classes.GridRoot }}>
      <Grid item>{getPanelTitle()}</Grid>
      <Grid item xs>
        {getWeekPicker()}
      </Grid>
      <Grid item>{getAddButton()}</Grid>
    </Grid>
  );

  function getAddButton() {
    return (
      <IconButton
        onClick={openDialog}
        classes={{ root: classes.IconButtonAdd }}
      >
        <AddCircleOutlineIcon classes={{ root: classes.IconAdd }} />
      </IconButton>
    );
  }

  function getWeekPicker() {
    return (
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <DatePicker
          TextFieldComponent={getButton}
          open={open}
          onOpen={() => setOpen(true)}
          onClose={() => setOpen(false)}
          value={moment().isoWeek(week)}
          onChange={handleWeekChenge}
          disableToolbar
          showTodayButton
          todayLabel='Сегодня'
          cancelLabel='Отмена'
          okLabel='Выбрать'
          renderDay={renderWrappedWeekDay}
        />
      </MuiPickersUtilsProvider>
    );
  }

  function getButton() {
    return (
      <Button
        onClick={() => setOpen(true)}
        variant='outlined'
        classes={{
          root: classes.ButtonRootWeek,
          label: classes.ButtonLabelWeek,
        }}
      >
        <Typography variant='h2'>
          {moment().isoWeek(week).format('W')}
        </Typography>
        <Typography variant='subtitle1'>неделя</Typography>
      </Button>
    );
  }

  function renderWrappedWeekDay(date, selectedDate, dayInCurrentMonth) {
    const start = moment(selectedDate).startOf('week');
    const end = moment(selectedDate).endOf('week');

    const dayIsBetween = moment(date).isBetween(start, end);
    const isFirstDay = moment(date).isSame(start, 'day');
    const isLastDay = moment(date).isSame(end, 'day');
    const isHoliday = [5, 6].indexOf(moment(date).weekday()) !== -1;

    const wrapperClassName = clsx({
      [classes.highlight]: dayIsBetween || isFirstDay || isLastDay,
      [classes.firstHighlight]: isFirstDay,
      [classes.endHighlight]: isLastDay,
      [classes.holiday]: isHoliday,
    });

    const dayClassName = clsx(classes.day, {
      [classes.nonCurrentMonthDay]: !dayInCurrentMonth,
      [classes.highlightNonCurrentMonthDay]: !dayInCurrentMonth && dayIsBetween,
    });

    return (
      <div className={wrapperClassName}>
        <IconButton className={dayClassName}>
          <span> {moment(date).format('D')} </span>
        </IconButton>
      </div>
    );
  }

  function getPanelTitle() {
    return (
      <Typography variant='h4' align='right' classes={{ root: classes.Title }}>
        Меню
      </Typography>
    );
  }
}

export default MenuPanel;
