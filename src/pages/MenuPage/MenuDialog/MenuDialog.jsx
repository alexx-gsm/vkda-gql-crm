import React from 'react';
import { Grid, FormControlLabel, Switch, Typography } from '@material-ui/core';
import { Container, Dialog, DialogContent, Slide } from '@material-ui/core';
import { Paper, AppBar, Toolbar, IconButton, Button } from '@material-ui/core';
import { Checkbox, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import FormDatePicker from '../../../components/form/FormDatePicker';
import MenuDialogDishesList from './MenuDialogDishesList';

import { useMenuPageContext } from '../menuPageContext';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { UPDATE_MENU } from '../../../graphql/mutation';
import { GET_DISHES } from '../../../graphql/query';
import MenuPrintDialog from '../MenuPrintDialog';

import moment from 'moment';

import { arrToObj } from '../../../utils/arrToObj';
import isEmpty from '../../../utils/is-empty';
import useStyles from './styles';

function MenuDialog() {
  const classes = useStyles();
  const [setError] = React.useState({});
  const {
    state: { isOpenDialog, menu, menus, dndState, autoSelectedDishes },
    addDishes,
    setValue,
    setFilter,
    setDndState,
    storeMenus,
    closeDialog,
    setAutoSelectedDishes,
  } = useMenuPageContext();

  const { data: dishData, loading: dishLoading } = useQuery(GET_DISHES);

  const [update] = useMutation(UPDATE_MENU, {
    onCompleted: onCompletedMutationHandler,
    onError: (error) => console.log('edit dish error:', error),
  });

  const handleChangeSwitch = (e) => setValue('isActive')(e.target.checked);

  const handleMenuCopy = () => {
    setValue('_id')(null);
    setValue('isActive')(false);
    setValue('date')(moment());
    setFilter('week')(moment().isoWeek());
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const { categories, categoryOrder } = dndState;

    const dishes = categoryOrder.reduce((acc, cat) => {
      return [
        ...acc,
        ...categories[cat._id].dishes.map(({ __typename, ...dish }) => {
          const { __typename: _, ...category } = dish.category;
          return {
            ...dish,
            category,
          };
        }),
      ];
    }, []);

    if (!dishes?.length) {
      return;
    }

    update({
      variables: {
        ...menu,
        dishes,
      },
    });
  };

  /** MENU PRINT DIALOG STATE */
  const [printDialogOpen, setPrintDialogOpen] = React.useState(false);

  const handlePrintDialogOpen = () => setPrintDialogOpen(true);

  const handlePrintDialogClose = () => setPrintDialogOpen(false);

  return (
    <>
      <Dialog
        fullScreen
        scroll='body'
        TransitionComponent={Slide}
        TransitionProps={{ direction: 'up' }}
        onClose={closeDialog}
        open={isOpenDialog}
      >
        {getAppBar()}
        <DialogContent classes={{ root: classes.Content }}>
          <Container maxWidth='md'>
            <Paper
              variant='outlined'
              square
              classes={{ root: classes.AddPaper }}
            >
              {getContentPanel()}
              {getAddDishesDialog()}
            </Paper>
            <MenuDialogDishesList />
          </Container>
        </DialogContent>
      </Dialog>
      <MenuPrintDialog
        open={printDialogOpen}
        handleClose={handlePrintDialogClose}
      />
    </>
  );

  function getAppBar() {
    return (
      <AppBar className={classes.AppBar} color='secondary'>
        <Toolbar variant='dense'>
          <IconButton
            edge='start'
            color='inherit'
            onClick={closeDialog}
            aria-label='close'
          >
            <CloseIcon />
          </IconButton>
          <Typography variant='h6' className={classes.Title}>
            Меню
          </Typography>
          <Button
            color='inherit'
            size='small'
            classes={{ root: classes.CopyButton }}
            disabled={!menu._id}
            onClick={handleMenuCopy}
          >
            Копировать
          </Button>
          <Button
            color='inherit'
            size='small'
            classes={{ root: classes.CopyButton }}
            disabled={!menu._id}
            onClick={handlePrintDialogOpen}
          >
            Печать
          </Button>
          <Button
            color='inherit'
            variant='outlined'
            disabled={isEmpty(dndState?.dishes)}
            onClick={handleSubmit}
          >
            Сохранить
          </Button>
        </Toolbar>
      </AppBar>
    );
  }

  function getContentPanel() {
    return (
      <Grid container spacing={2} alignItems='center'>
        <Grid item>
          <FormDatePicker
            date={menu.date}
            handleDateChange={setValue('date')}
          />
        </Grid>
        <Grid item>
          <FormControlLabel
            control={
              <Switch
                checked={menu.isActive}
                onChange={handleChangeSwitch}
                color='secondary'
              />
            }
            label='Опубликовать'
          />
        </Grid>
      </Grid>
    );
  }

  function getAddDishesDialog() {
    if (dishLoading || !dishData) {
      return '';
    }

    const dishes = dishData.dishes.data;
    const icon = <CheckBoxOutlineBlankIcon fontSize='small' />;
    const checkedIcon = <CheckBoxIcon fontSize='small' />;

    return (
      <Grid container spacing={2} alignItems='flex-end'>
        <Grid item xs={11}>
          <Autocomplete
            multiple
            autoComplete
            fullWidth
            options={dishes.sort(
              (a, b) => a.category.sorting - b.category.sorting,
            )}
            groupBy={(opt) => opt.category.title}
            value={autoSelectedDishes}
            onChange={(e, items) => setAutoSelectedDishes(items)}
            disableCloseOnSelect
            openText='Открыть'
            noOptionsText='не найдено'
            getOptionLabel={(option) => option.title}
            filterOptions={(opt, { inputValue }) => {
              const availbleDishes = opt.filter(
                ({ _id }) => !dndState.dishes.hasOwnProperty(_id),
              );
              return inputValue.length
                ? availbleDishes.filter(
                    (d) =>
                      d.title
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) !== -1,
                  )
                : availbleDishes;
            }}
            renderOption={(option, { selected }) => (
              <React.Fragment>
                <Checkbox
                  icon={icon}
                  checkedIcon={checkedIcon}
                  style={{ marginRight: 8 }}
                  checked={selected}
                />
                {option.title}
              </React.Fragment>
            )}
            renderInput={(params) => (
              <TextField {...params} label='Блюда' margin='dense' />
            )}
          />
        </Grid>
        <Grid item>
          <IconButton
            size='small'
            onClick={addDishes}
            disabled={!autoSelectedDishes.length}
          >
            <AddCircleIcon
              fontSize='large'
              color={autoSelectedDishes.length ? 'secondary' : 'disabled'}
            />
          </IconButton>
        </Grid>
      </Grid>
    );
  }

  function onCompletedMutationHandler({ updateMenu }) {
    if (updateMenu.isSuccess) {
      const _updatedMenu = updateMenu.data[0];

      let _updatedMenus;

      if (menu._id) {
        _updatedMenus = menus.map((m) =>
          m._id === _updatedMenu._id ? _updatedMenu : m,
        );
      } else {
        _updatedMenus = [...menus, _updatedMenu];
      }

      storeMenus(_updatedMenus);

      const week = moment(_updatedMenu.date).isoWeek();

      setFilter('week')(week);
      setDndState(null);
      closeDialog();
    } else {
      if (updateMenu.errors.length) {
        setError(arrToObj(updateMenu.errors));
      }
    }
  }
}

export default MenuDialog;
