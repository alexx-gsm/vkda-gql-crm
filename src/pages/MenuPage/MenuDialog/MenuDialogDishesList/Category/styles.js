import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Paper: {
    padding: theme.spacing(0, 2, 1),
    margin: theme.spacing(2, 0),
  },
}));
