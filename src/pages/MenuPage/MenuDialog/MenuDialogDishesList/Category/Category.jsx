import React from 'react';
import { Paper, Typography } from '@material-ui/core';
import Dish from '../Dish';
import { Droppable } from 'react-beautiful-dnd';

import useStyles from './styles';

function Category({ category }) {
  const classes = useStyles();

  return category.dishes.length ? (
    <Paper
      key={category._id}
      square
      variant='elevation'
      classes={{ root: classes.Paper }}
    >
      <Typography variant='h6' color='secondary' align='center'>
        {category.title}
      </Typography>

      <Droppable droppableId={category._id}>
        {(provided) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {category?.dishes.map((dish, index) => (
              <Dish key={dish._id} dish={dish} index={index} />
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </Paper>
  ) : (
    ''
  );
}

export default Category;
