import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  DishContainer: {
    marginBottom: theme.spacing(1),
  },
  Separator: {
    borderBottom: `1px dotted ${theme.palette.grey[400]}`,
    height: '100%',
  },
  TextItalic: {
    fontStyle: 'italic',
  },
}));
