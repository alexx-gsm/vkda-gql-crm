import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { Grid, Typography, IconButton, Box } from '@material-ui/core';
import { TextField, InputAdornment } from '@material-ui/core';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

import { useMenuPageContext } from '../../../menuPageContext';

import useStyles from './styles';

function Dish({ dish, index }) {
  const classes = useStyles();

  const { editDish, deleteDish } = useMenuPageContext();

  const handleDelete = (dish) => deleteDish(dish);

  const handleChange = (field) => (e) => {
    editDish({
      ...dish,
      [field]: e.target.value,
    });
  };

  return (
    <Draggable draggableId={dish._id} index={index}>
      {(provided) => (
        <Grid
          key={dish._id}
          container
          direction='column'
          classes={{ root: classes.DishContainer }}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
        >
          <Grid item container spacing={1}>
            <Grid item style={{ alignSelf: 'center', width: '35px' }}>
              <IconButton size='small' onClick={() => handleDelete(dish)}>
                <HighlightOffIcon color='secondary' />
              </IconButton>
            </Grid>
            <Grid item xs={1}>
              <Typography variant='h6' align='center'>
                {dish.index}.
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant='h5'>{dish.title}</Typography>
            </Grid>
            <Grid item xs>
              <Box classes={{ root: classes.Separator }} />
            </Grid>
            <Grid item xs={2} sm={1}>
              <TextField
                value={dish.weight}
                onChange={handleChange('weight')}
                margin='none'
              />
            </Grid>
            <Grid item xs={2} sm={1}>
              <TextField
                value={dish.price}
                onChange={handleChange('price')}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position='end'>р</InputAdornment>
                  ),
                }}
                margin='none'
              />
            </Grid>
          </Grid>
          {dish.composition && (
            <Grid container>
              <Grid item style={{ width: '35px' }}></Grid>
              <Grid item xs={1} />
              <Grid item>
                <Typography
                  variant='caption'
                  classes={{ root: classes.TextItalic }}
                >
                  ({dish.composition})
                </Typography>
              </Grid>
            </Grid>
          )}
        </Grid>
      )}
    </Draggable>
  );
}

export default Dish;
