import React from 'react';
import { Typography } from '@material-ui/core';
import Category from './Category';
import { Skeleton } from '@material-ui/lab';
import { DragDropContext } from 'react-beautiful-dnd';

import { useMenuPageContext } from '../../menuPageContext';
import { useQuery } from '@apollo/react-hooks';
import { GET_CATEGORIES } from '../../../../graphql/query/categories';

function MenuDialogDishesList() {
  const {
    state: { isOpenDialog, menu, dndState },
    setDndState,
  } = useMenuPageContext();

  const { data, loading } = useQuery(GET_CATEGORIES);
  const categories = data?.categories?.data;

  const sortedCategories = React.useCallback(() => {
    const dishCategory = categories?.find((c) => c.title === 'Блюда');

    if (!dishCategory) {
      return [];
    }

    return categories
      ?.filter((c) => c.parent === dishCategory._id)
      .sort((a, b) => a.sorting - b.sorting);
  }, [categories]);

  React.useEffect(() => {
    if (loading || !isOpenDialog || dndState) {
      return;
    }
    const { dishes } = menu;

    if (!dishes) {
      return;
    }

    const categories = sortedCategories();

    const _dndState = {
      dishes: dishes?.reduce((acc, dish) => {
        return {
          ...acc,
          [dish._id]: dish,
        };
      }, {}),
      categories: categories?.reduce((acc, category) => {
        return {
          ...acc,
          [category._id]: {
            ...category,
            dishes: dishes.filter((d) => d.category._id === category._id),
          },
        };
      }, {}),
      categoryOrder: categories,
    };
    // TODO: add callback and missing dependency
    setDndState(_dndState);
  }, [loading, menu, sortedCategories]);

  const onDragEnd = ({ destination, source, draggableId }) => {
    if (!destination) {
      return;
    }
    if (destination.droppableId !== source.droppableId) {
      return;
    }

    const dish = dndState?.dishes[draggableId];
    const dishes = Array.from(dndState.categories[source.droppableId].dishes);
    let index = dishes[0].index;

    dishes.splice(source.index, 1);
    dishes.splice(destination.index, 0, dish);

    const newDndState = {
      ...dndState,
      categories: {
        ...dndState.categories,
        [source.droppableId]: {
          ...dndState.categories[source.droppableId],
          dishes: dishes.map((d) => ({ ...d, index: index++ })),
        },
      },
    };

    setDndState(newDndState);
  };

  if (loading) {
    return <Skeleton />;
  }

  if (!dndState || !Object.keys(dndState.dishes).length) {
    return (
      <Typography variant='h6' align='center'>
        Пусто
      </Typography>
    );
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      {dndState.categoryOrder.map((sortedCategory) => {
        const dndCategory = dndState?.categories?.[sortedCategory._id];
        return <Category key={dndCategory?._id} category={dndCategory} />;
      })}
    </DragDropContext>
  );
}

export default MenuDialogDishesList;
