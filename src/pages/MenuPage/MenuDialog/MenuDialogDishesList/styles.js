import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Paper: {
    padding: theme.spacing(0, 2, 1),
    margin: theme.spacing(2, 0),
  },
  DishContainer: {
    marginBottom: theme.spacing(1),
  },
  Separator: {
    borderBottom: `1px dotted ${theme.palette.grey[400]}`,
  },
}));
