import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  AppBar: {
    position: 'relative',
  },
  Title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  Content: {
    padding: theme.spacing(3),
  },
  AddPaper: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[100],
  },
  AddButton: {
    fontSize: '4rem',
  },
  CopyButton: {
    marginRight: theme.spacing(2),
  },
  ErrorHelperText: {
    position: 'absolute',
    top: '100%',
    marginTop: 0,
  },
}));
