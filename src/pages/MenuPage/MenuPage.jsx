import React from 'react';
import { Grid } from '@material-ui/core';
import MenuContent from './MenuContent';
import MenuDialog from './MenuDialog';
import MenuPanel from './MenuPanel/MenuPanel';

import { MenuPageProvider } from './menuPageContext';

function MenuPage() {
  return (
    <MenuPageProvider>
      <Grid container wrap='nowrap' style={{ height: '100vh' }}>
        <Grid item container style={{ maxWidth: '200px' }}>
          <MenuPanel />
        </Grid>
        <Grid item xs>
          <MenuContent />
        </Grid>
      </Grid>
      <MenuDialog />
    </MenuPageProvider>
  );
}

export default MenuPage;
