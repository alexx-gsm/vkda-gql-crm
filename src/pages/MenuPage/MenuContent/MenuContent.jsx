import React from 'react';
import { Table, TableContainer, TableHead, TableBody } from '@material-ui/core';
import { TableRow, TableCell } from '@material-ui/core';
import { Paper, Typography } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import { withStyles } from '@material-ui/core/styles';

import { useQuery } from '@apollo/react-hooks';
import { useMenuPageContext } from '../menuPageContext';
import { GET_MENUS } from '../../../graphql/query';
import useStyles from './styles';
import moment from 'moment';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    cursor: 'pointer',
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const getTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell>Дата</StyledTableCell>
        <StyledTableCell>Опубликовано</StyledTableCell>
      </TableRow>
    </TableHead>
  );
};

function MenuContent() {
  const classes = useStyles();
  const {
    state: { filter, menus },
    storeMenu,
    storeMenus,
  } = useMenuPageContext();
  const { loading, refetch } = useQuery(GET_MENUS, {
    variables: {
      week: filter.week,
    },
    fetchPolicy: 'no-cache',
    onCompleted: ({ menus }) => {
      storeMenus(menus?.data || []);
    },
  });

  React.useEffect(() => {
    if (loading) {
      return;
    }

    refetch({ week: filter.week });
  }, [refetch, loading, filter.week]);

  if (loading || !menus) {
    return (
      <Typography variant='h6' align='center'>
        loading...
      </Typography>
    );
  }

  // const menus = state?.menus || [];

  const handleClick = (menu) => () => {
    storeMenu(menu);
  };

  return (
    <TableContainer
      component={Paper}
      classes={{ root: classes.TableContainer }}
    >
      <Table size='small'>
        {getTableHead()}
        <TableBody>
          {menus &&
            menus
              .sort((a, b) => new Date(a.date) - new Date(b.date))
              .map((menu) => {
                return (
                  <StyledTableRow key={menu._id} onClick={handleClick(menu)}>
                    <TableCell style={{ width: '100%' }}>
                      <Typography variant='caption'>
                        {moment(menu.date).format('dd:')}
                      </Typography>
                      <Typography
                        variant='h6'
                        classes={{ root: classes.TypoDate }}
                      >
                        {moment(menu.date).format('DD MMMM YYYY')}г.
                      </Typography>
                    </TableCell>
                    <TableCell align='center'>
                      {menu.isActive ? <DoneIcon color='secondary' /> : ''}
                    </TableCell>
                  </StyledTableRow>
                );
              })}
        </TableBody>
      </Table>
      {menus.length === 0 && (
        <Typography variant='h6' align='center'>
          Нет данных
        </Typography>
      )}
    </TableContainer>
  );
}

export default MenuContent;
