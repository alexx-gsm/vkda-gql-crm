import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  TableContainer: {
    borderRadius: 0,
    maxWidth: '1200px',
  },
  TypoDate: {
    display: 'inline-block',
    marginLeft: theme.spacing(1),
  },
}));
