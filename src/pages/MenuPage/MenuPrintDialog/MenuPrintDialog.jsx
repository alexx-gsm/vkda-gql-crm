import React from 'react';
import Button from '@material-ui/core/Button';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';

import { useMenuPageContext } from '../menuPageContext';

import moment from 'moment';
import formatter from '../../../utils/formatter';
import { exportComponentAsJPEG } from 'react-component-export-image';
import useStyles from './styles';
import './styles.css';

import RU_MONTHS from './ru-months';

const SELECTED_CATEGORIES = ['супы', 'горячее', 'гарниры'];
const BLOCK_CATEGORIES = ['напитки'];
const MIN_ORDER = 200;
const DELIVERY = 100;

const MenuPrint = React.forwardRef((props, ref) => {
  const { menu, dndState } = props;
  const { categoryOrder, categories } = dndState;
  const _categories = categoryOrder.filter(
    (c) => BLOCK_CATEGORIES.indexOf(c.title.toLowerCase()) === -1,
  );
  const __categories = categoryOrder.filter(
    (c) => BLOCK_CATEGORIES.indexOf(c.title.toLowerCase()) !== -1,
  );

  console.log('_categories', _categories);
  console.log('__categories', __categories);

  return (
    <div ref={ref} className='container'>
      <div className='wrapper'>
        <div className='head'>
          <div className='head-image-wrap'>
            <img src='logo.png' className='head-image' alt='' />
          </div>
          <h1 className='head-title'>{moment(menu?.date).format('dddd')}</h1>
          <div className='head-date-wrap'>
            <div className='head-date-day'>
              {moment(menu?.date).format('D')}
            </div>
            <div className='head-date-month'>
              {RU_MONTHS[moment(menu?.date).format('M') - 1]}
            </div>
          </div>
        </div>
        <div className='content'>
          <div className='menu'>
            {_categories.map((cat) => {
              if (!categories[cat._id].dishes.length) {
                return null;
              }
              const marked =
                SELECTED_CATEGORIES.indexOf(cat.title.toLowerCase()) !== -1;
              return (
                <div key={cat._id} className='category'>
                  <h2 className='category-title'>
                    {categories[cat._id].title}
                    {marked && <span>*</span>}
                  </h2>
                  <div className='category-dishes'>
                    {categories[cat._id].dishes.map((d) => {
                      return (
                        <div key={d._id} className='dish'>
                          <div className='dish-index'>{d.index}.</div>

                          <div className='dish-desc-wrap'>
                            <div className='dish-desc'>
                              <div className='dish-title'>{d.title}</div>
                              <div className='dish-weight'>{d.weight}</div>
                              <div className='dish-price'>
                                {formatter.format(d.price)}
                              </div>
                            </div>
                            {d.composition && (
                              <div className='dish-composition'>
                                {d.composition}
                              </div>
                            )}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}
            <div className='menu-block'>
              {__categories.map((cat) => {
                if (!categories[cat._id].dishes.length) {
                  return null;
                }
                const marked =
                  SELECTED_CATEGORIES.indexOf(cat.title.toLowerCase()) !== -1;
                return (
                  <div key={cat._id} className='category'>
                    <h2 className='category-title'>
                      {categories[cat._id].title}
                      {marked && <span>*</span>}
                    </h2>
                    <div className='category-dishes'>
                      {categories[cat._id].dishes.map((d) => {
                        return (
                          <div key={d._id} className='dish'>
                            <div className='dish-index'>{d.index}.</div>

                            <div className='dish-desc-wrap'>
                              <div className='dish-desc'>
                                <div className='dish-title'>{d.title}</div>
                                <div className='dish-weight'>{d.weight}</div>
                                <div className='dish-price'>
                                  {formatter.format(d.price)}
                                </div>
                              </div>
                              {d.composition && (
                                <div className='dish-composition'>
                                  {d.composition}
                                </div>
                              )}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className='info'>
            <div className='block'>
              <div className='title'>прием заявок</div>
              <div className='subtitle mb-1'>
                пн-пт <b>8:00-10:00</b>
              </div>
              <div className='subtitle'>по телефону:</div>
              <div className='value'>99-88-80</div>
              <div className='subtitle'>WhatsApp:</div>
              <div className='value small'>8-964-086-8880</div>
            </div>
            <div className='block'>
              <div className='title delivery'>Бесплатная</div>
              <div className='subtitle'>доставка</div>
              <div className='subtitle'>
                в <b>г.Барнауле</b>
              </div>
              <div className='subtitle'>при заказе от</div>
              <div className='value'>{formatter.format(MIN_ORDER)}</div>
            </div>
            {/* <div className='block block-promo'>
              <div className='block-image-wrap'>
                <img
                  src='assets/images/promo-20.jpg'
                  className='block-image'
                  alt=''
                />
              </div>
              <div className='subtitle'>На все блюда!</div>
            </div> */}
          </div>
        </div>
        <div className='footer'>
          <div className='note note-1'>
            Доплата за упаковку - <b>{formatter.format(4)}</b>
          </div>
          <div className='note note-2'>
            {`При заказе менее ${formatter.format(
              MIN_ORDER,
            )} стоимость доставки - `}
            <b>{formatter.format(DELIVERY)}</b>
          </div>
        </div>
      </div>
    </div>
  );
});

function MenuPrintDialog({ open, handleClose }) {
  const classes = useStyles();
  const componentRef = React.useRef();

  const {
    state: { menu, dndState },
  } = useMenuPageContext();

  const handlePrint = () => exportComponentAsJPEG(componentRef);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth='md'
      scroll='paper'
      classes={{ root: classes.Dialog }}
    >
      <DialogContent classes={{ root: classes.Content }}>
        <MenuPrint ref={componentRef} menu={menu} dndState={dndState} />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Отмена</Button>
        <Button onClick={handlePrint} color='primary' variant='contained'>
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  );
}
export default MenuPrintDialog;
