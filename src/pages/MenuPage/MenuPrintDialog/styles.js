import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Dialog: {
    display: 'flex',
    justifyContent: 'center',
  },
  Content: {
    padding: theme.spacing(1),
    '&:first-child': {
      paddingTop: theme.spacing(1),
    },
  },
}));
