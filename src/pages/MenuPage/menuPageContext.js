import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';
import moment from 'moment';

const MODULE_NAME = `${APP_NAME}/MENU`;

// constants
const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const FILTER_SET = `${MODULE_NAME}/FILTER_SET`;
const FILTER_CLEAR = `${MODULE_NAME}/FILTER_CLEAR`;
const SET_MENUS = `${MODULE_NAME}/SET_MENUS`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;
const SET_VALUE = `${MODULE_NAME}/SET_VALUE`;
const SET_DND_STATE = `${MODULE_NAME}/SET_DND_STATE`;
const SET_AUTO_SELECTED_DISHES = `${MODULE_NAME}/SET_AUTO_SELECTED_DISHES`;
// context
const MenuPageContext = React.createContext();
// initial
const emptyMenu = {
  date: moment(),
  isActive: true,
  dishes: [],
};
const emptyFilter = {
  week: moment().isoWeek(),
};
// immutable initial state Record
const ReducerRecord = Record({
  isOpenDialog: false,
  menus: null,
  menu: emptyMenu,
  dndState: null,
  autoSelectedDishes: [],
  filter: emptyFilter,
});

/**
 * reducer
 */
function menuPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state
        .set('isOpenDialog', false)
        .set('menu', emptyMenu)
        .set('dndState', null);
    }
    case UPDATE_RECORD: {
      return state.setIn(['menu', payload.key], payload.value);
    }
    case FILTER_SET: {
      return state.setIn(['filter', payload.key], payload.value);
    }
    case FILTER_CLEAR: {
      return state.set('filter', emptyFilter);
    }
    case SET_MENUS: {
      return state.set('menus', payload.menus);
    }
    case SET_ITEM: {
      return state.set('menu', payload.menu).set('isOpenDialog', true);
    }
    case SET_VALUE: {
      return state.setIn(['menu', payload.key], payload.value);
    }
    case SET_DND_STATE: {
      return state.set('dndState', payload.dndState);
    }
    case SET_AUTO_SELECTED_DISHES: {
      return state.set('autoSelectedDishes', payload.autoSelectedDishes);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 * provider
 */
function MenuPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    menuPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <MenuPageContext.Provider value={value} {...props} />;
}

/**
 * context
 */
function useMenuPageContext() {
  const context = React.useContext(MenuPageContext);
  if (!context) {
    throw new Error(
      `useMenuPageContext must be used within a MenuPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateMenu = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const setFilter = (key) => (value) =>
    dispatch({ type: FILTER_SET, payload: { key, value } });
  const clearFilter = () => dispatch({ type: FILTER_CLEAR });
  const storeMenu = (menu) => dispatch({ type: SET_ITEM, payload: { menu } });
  const setValue = (key) => (value) =>
    dispatch({ type: SET_VALUE, payload: { key, value } });
  const setDndState = (dndState) =>
    dispatch({ type: SET_DND_STATE, payload: { dndState } });
  const setAutoSelectedDishes = (autoSelectedDishes) =>
    dispatch({
      type: SET_AUTO_SELECTED_DISHES,
      payload: { autoSelectedDishes },
    });

  /**
   * *SET MENUS */
  const storeMenus = (menus) =>
    dispatch({ type: SET_MENUS, payload: { menus } });

  /**
   ** ADD DISH
   */
  const addDishes = () => {
    const { categories, dishes, categoryOrder } = state.dndState;

    const updatedDishes = {
      ...dishes,
      ...state.autoSelectedDishes.reduce(
        (acc, item) => ({ ...acc, [item._id]: item }),
        {},
      ),
    };

    let index = 1;

    const updatedCategories = categoryOrder.reduce((acc, cat) => {
      const _dishes = [
        ...categories[cat._id].dishes,
        ...state.autoSelectedDishes.filter(
          (item) => item.category._id === cat._id,
        ),
      ];
      return {
        ...acc,
        [cat._id]: {
          ...cat,
          dishes: _dishes.map((d) => ({ ...d, index: index++ })),
        },
      };
    }, {});

    dispatch({
      type: SET_DND_STATE,
      payload: {
        dndState: {
          dishes: updatedDishes,
          categories: updatedCategories,
          categoryOrder,
        },
      },
    });

    dispatch({
      type: SET_AUTO_SELECTED_DISHES,
      payload: { autoSelectedDishes: [] },
    });
  };

  /**
   ** EDIT DISH
   */
  const editDish = (dish) => {
    const { categories, dishes, categoryOrder } = state.dndState;

    const updatedDishes = {
      ...dishes,
      [dish._id]: dish,
    };

    const updatedCategories = categoryOrder.reduce((acc, cat) => {
      const _dishes =
        dish.category._id === cat._id
          ? categories[cat._id].dishes.map((d) =>
              d._id === dish._id ? dish : d,
            )
          : categories[cat._id].dishes;

      return {
        ...acc,
        [cat._id]: {
          ...cat,
          dishes: _dishes,
        },
      };
    }, {});

    dispatch({
      type: SET_DND_STATE,
      payload: {
        dndState: {
          dishes: updatedDishes,
          categories: updatedCategories,
          categoryOrder,
        },
      },
    });
  };

  /**
   ** DELETE DISH
   */
  const deleteDish = (dish) => {
    const { categories, dishes, categoryOrder } = state.dndState;
    const { [dish._id]: _, ...updatedDishes } = dishes;

    let index = 1;

    const updatedCategories = categoryOrder.reduce((acc, cat) => {
      const _dishes = categories[cat._id].dishes.filter(
        (d) => d._id !== dish._id,
      );
      return {
        ...acc,
        [cat._id]: {
          ...cat,
          dishes: _dishes.map((d) => ({ ...d, index: index++ })),
        },
      };
    }, {});

    dispatch({
      type: SET_DND_STATE,
      payload: {
        dndState: {
          dishes: updatedDishes,
          categories: updatedCategories,
          categoryOrder,
        },
      },
    });
  };

  return {
    state,
    openDialog,
    closeDialog,
    updateMenu,
    setFilter,
    clearFilter,
    storeMenu,
    storeMenus,
    setValue,
    setDndState,
    addDishes,
    editDish,
    deleteDish,
    setAutoSelectedDishes,
  };
}

export { MenuPageProvider, useMenuPageContext };
