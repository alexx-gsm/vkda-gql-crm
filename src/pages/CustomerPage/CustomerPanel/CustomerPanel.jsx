import React from 'react';
import { Grid, Button, Typography, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SearchInput from '../../../components/common/SearchInput/SearchInput';

import { useCustomerPageContext } from '../customerPageContext';

import useStyles from './styles';

function CustomerPanel() {
  const classes = useStyles();
  const {
    state,
    openDialog,
    setFilter,
    clearFilter,
  } = useCustomerPageContext();

  const { searchFilter } = state.filter;

  return (
    <Grid
      container
      alignItems='flex-end'
      spacing={2}
      classes={{ root: classes.GridRoot }}
    >
      <Grid item>
        <Button variant='contained' color='secondary' onClick={openDialog}>
          Добавить
        </Button>
      </Grid>
      <Grid item>
        <SearchInput
          value={searchFilter}
          handleChange={setFilter('searchFilter')}
        />
      </Grid>
      <Grid item>
        <IconButton size='small' disabled={!searchFilter} onClick={clearFilter}>
          <CloseIcon fontSize='small' />
        </IconButton>
      </Grid>
      <Grid item xs>
        <Typography variant='h4' align='right'>
          Клиенты
        </Typography>
      </Grid>
    </Grid>
  );
}

export default CustomerPanel;
