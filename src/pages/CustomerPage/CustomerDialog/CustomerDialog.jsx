import React from 'react';
import { Grid, TextField, IconButton } from '@material-ui/core';
import { Dialog, DialogContent } from '@material-ui/core';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import AddBoxIcon from '@material-ui/icons/AddBox';
import FormDialogActions from '../../../components/form/FormDialogActions';
import FormDialogTitle from '../../../components/form/FormDialogTitle';
import CompanySelect from '../../../components/form/CompanySelect';
import Skeleton from '@material-ui/lab/Skeleton';

import { useQuery, useMutation } from '@apollo/react-hooks';
import { useCustomerPageContext } from '../customerPageContext';
import { UPDATE_CUSTOMER } from '../../../graphql/mutation';
import { GET_CUSTOMERS, GET_COURIERS } from '../../../graphql/query';

import { arrToObj } from '../../../utils/arrToObj';
import useStyles from './styles';

function CustomerDialog() {
  const classes = useStyles();

  const [error, setError] = React.useState({});
  const {
    state: { isOpenDialog, customer },
    closeDialog,
    updateCustomer,
  } = useCustomerPageContext();

  const { data: couriersData, loading: couriersLoadingData } = useQuery(
    GET_COURIERS,
  );
  const couriers = couriersData?.couriers?.data || [];

  const [update, { loading }] = useMutation(UPDATE_CUSTOMER, {
    onCompleted: onCompletedMutationHandler,
    update: updateCache,
    onError: (error) => console.log('edit customer error:', error),
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    update({ variables: { ...customer, company: customer.company._id } });
  };

  return (
    <Dialog
      maxWidth='sm'
      fullWidth
      open={isOpenDialog}
      onClose={closeDialog}
      classes={{ paper: classes.Paper }}
    >
      <FormDialogTitle title='Клиент' />
      <DialogContent classes={{ root: classes.Content }}>
        <TextField
          autoFocus
          required
          id='title'
          label='ФИО'
          margin='normal'
          variant='outlined'
          fullWidth
          value={customer?.name || ''}
          onChange={updateCustomer('name')}
          error={!!error?.name}
          helperText={error?.name || ''}
          FormHelperTextProps={{ classes: { root: classes.ErrorHelperText } }}
        />
        <Grid container spacing={2} alignItems='center'>
          <Grid item xs>
            <CompanySelect
              company={customer?.company || ''}
              setCompany={updateCustomer('company')}
            />
          </Grid>
          <Grid item>
            <IconButton size='small' disableRipple>
              <AddBoxIcon classes={{ root: classes.ButtonAdd }} />
            </IconButton>
          </Grid>
        </Grid>
        <TextField
          label='Дополнительно'
          margin='normal'
          variant='outlined'
          fullWidth
          value={customer?.address || ''}
          onChange={updateCustomer('address')}
        />
        <TextField
          required
          label='Телефон'
          margin='normal'
          variant='outlined'
          fullWidth
          value={customer?.phone || ''}
          onChange={updateCustomer('phone')}
        />
        <Grid container spacing={2}>
          <Grid item xs>
            {getPaymentTypeSelect()}
          </Grid>
          <Grid item xs>
            <TextField
              label='Скидка'
              margin='normal'
              variant='outlined'
              fullWidth
              type='number'
              value={+customer?.discount || 0}
              onChange={updateCustomer('discount')}
            />
          </Grid>
        </Grid>
        {getCourierSelect()}
        <TextField
          id='comment'
          label='Комментарий'
          margin='normal'
          variant='outlined'
          multiline
          rows={3}
          fullWidth
          value={customer.comment || ''}
          onChange={updateCustomer('comment')}
          error={!!error?.comment}
          helperText={error?.comment || ''}
          FormHelperTextProps={{
            classes: { root: classes.ErrorHelperText },
          }}
        />
      </DialogContent>
      <FormDialogActions
        loading={loading}
        handleSubmit={handleSubmit}
        handleClose={closeDialog}
      />
    </Dialog>
  );

  function getPaymentTypeSelect() {
    return (
      <FormControl
        margin='normal'
        variant='outlined'
        fullWidth
        className={classes.formControl}
      >
        <InputLabel htmlFor='paymentType'>Тип оплаты</InputLabel>
        <Select
          label='Тип оплаты'
          value={customer?.paymentType}
          onChange={updateCustomer('paymentType')}
          inputProps={{
            name: 'paymentType',
            id: 'paymentType',
          }}
        >
          <MenuItem value='cash'>Наличные</MenuItem>
          <MenuItem value='card'>Карта</MenuItem>
          <MenuItem value='account'>Договор</MenuItem>
        </Select>
      </FormControl>
    );
  }

  function getCourierSelect() {
    return couriersLoadingData ? (
      <Skeleton />
    ) : (
      <FormControl
        margin='normal'
        variant='outlined'
        fullWidth
        className={classes.formControl}
      >
        <InputLabel htmlFor='courier'>Курьер</InputLabel>
        <Select
          label='Курьер'
          value={customer?.courier || ''}
          onChange={updateCustomer('courier')}
          inputProps={{
            name: 'courier',
            id: 'courier',
          }}
        >
          <MenuItem value=''>
            <em>-</em>
          </MenuItem>
          {couriers.map((c) => (
            <MenuItem key={c._id} value={c._id}>
              {c.title}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }

  function onCompletedMutationHandler({ updateCustomer }) {
    if (updateCustomer.isSuccess) {
      closeDialog();
    } else {
      if (updateCustomer.errors.length) {
        setError(arrToObj(updateCustomer.errors));
      }
    }
  }

  function updateCache(cache, { data }) {
    if (customer._id || !data.updateCustomer.isSuccess) {
      return;
    }
    const items = cache.readQuery({ query: GET_CUSTOMERS });
    const customers = items.customers ? items.customers.data : [];

    cache.writeQuery({
      query: GET_CUSTOMERS,
      data: {
        customers: {
          ...items.customers,
          data: [...customers, ...data.updateCustomer.data],
        },
      },
    });
  }
}

export default CustomerDialog;
