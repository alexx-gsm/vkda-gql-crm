import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  Paper: {
    padding: theme.spacing(2, 0),
  },
  ErrorHelperText: {
    position: 'absolute',
    top: '100%',
    marginTop: 0,
  },
  ButtonAdd: {
    fontSize: '61px',
    marginTop: theme.spacing(1),
    marginRight: -theme.spacing(1),
  },
}));
