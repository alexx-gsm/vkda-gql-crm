import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';

const MODULE_NAME = `${APP_NAME}/CUSTOMER`;

const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;
const FILTER_SET = `${MODULE_NAME}/FILTER_SET`;
const FILTER_CLEAR = `${MODULE_NAME}/FILTER_CLEAR`;

const CustomerPageContext = React.createContext();

const emptyCustomer = {
  name: '',
  company: {},
  paymentType: 'cash',
  discount: '0',
  address: '',
  courier: '',
  comment: '',
};

const emptyFilter = {
  searchFilter: '',
};

/**
 ** State initial
 */
const ReducerRecord = Record({
  isOpenDialog: false,
  customer: emptyCustomer,
  filter: emptyFilter,
});

/**
 ** reducer
 */
function customerPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state.set('isOpenDialog', false).set('customer', emptyCustomer);
    }
    case UPDATE_RECORD: {
      return state.setIn(['customer', payload.key], payload.value);
    }
    case SET_ITEM: {
      return state.set('customer', payload.customer).set('isOpenDialog', true);
    }
    case FILTER_SET: {
      return state.setIn(['filter', payload.key], payload.value);
    }
    case FILTER_CLEAR: {
      return state.set('filter', emptyFilter);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 ** provider
 */
function CustomerPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    customerPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <CustomerPageContext.Provider value={value} {...props} />;
}

/**
 ** context
 */
function useCustomerPageContext() {
  const context = React.useContext(CustomerPageContext);
  if (!context) {
    throw new Error(
      `useCustomerPageContext must be used within a CustomerPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateCustomer = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const setCustomer = (customer) =>
    dispatch({ type: SET_ITEM, payload: { customer } });
  const setFilter = (key) => (e) =>
    dispatch({ type: FILTER_SET, payload: { key, value: e.target.value } });
  const clearFilter = () => dispatch({ type: FILTER_CLEAR });

  return {
    state,
    openDialog,
    closeDialog,
    updateCustomer,
    setCustomer,
    setFilter,
    clearFilter,
  };
}

export { CustomerPageProvider, useCustomerPageContext };
