import React from 'react';
import { Container } from '@material-ui/core';
import CustomerPanel from './CustomerPanel';
import CustomerContent from './CustomerContent';
import CustomerDialog from './CustomerDialog';

import { CustomerPageProvider } from './customerPageContext';

function CustomerPage() {
  return (
    <Container maxWidth='md' disableGutters>
      <CustomerPageProvider>
        <CustomerPanel />
        <CustomerContent />
        <CustomerDialog />
      </CustomerPageProvider>
    </Container>
  );
}

export default CustomerPage;
