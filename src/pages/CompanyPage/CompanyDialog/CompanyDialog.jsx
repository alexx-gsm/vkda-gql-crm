import React from 'react';
import { TextField } from '@material-ui/core';
import { Dialog, DialogContent } from '@material-ui/core';
import FormDialogActions from '../../../components/form/FormDialogActions';
import FormDialogTitle from '../../../components/form/FormDialogTitle';

import { useMutation } from '@apollo/react-hooks';
import { useCompanyPageContext } from '../companyPageContext';
import { UPDATE_COMPANY } from '../../../graphql/mutation';
import { GET_COMPANIES } from '../../../graphql/query';

import { arrToObj } from '../../../utils/arrToObj';
import useStyles from './styles';

function CompanyDialog() {
  const classes = useStyles();
  const [error, setError] = React.useState({});
  const {
    state: { isOpenDialog, company },
    closeDialog,
    updateCompany,
  } = useCompanyPageContext();
  const [update, { loading }] = useMutation(UPDATE_COMPANY, {
    onCompleted: onCompletedMutationHandler,
    update: updateCache,
    onError: (error) => console.log('edit company error:', error),
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    update({ variables: company });
  };

  return (
    <Dialog
      maxWidth='sm'
      open={isOpenDialog}
      classes={{ paper: classes.Paper }}
    >
      <FormDialogTitle title='Организация' />
      <DialogContent classes={{ root: classes.Content }}>
        <TextField
          autoFocus
          required
          id='title'
          label='Название'
          margin='normal'
          variant='outlined'
          fullWidth
          value={company.title || ''}
          onChange={updateCompany('title')}
          error={!!error?.title}
          helperText={error?.title || ''}
          FormHelperTextProps={{ classes: { root: classes.ErrorHelperText } }}
        />
        <TextField
          required
          id='address'
          label='Адрес'
          margin='normal'
          variant='outlined'
          fullWidth
          value={company.address || ''}
          onChange={updateCompany('address')}
          error={!!error?.address}
          helperText={error?.address || ''}
          FormHelperTextProps={{ classes: { root: classes.ErrorHelperText } }}
        />
        <TextField
          id='comment'
          label='Комментарий'
          margin='normal'
          variant='outlined'
          multiline
          rows={3}
          fullWidth
          value={company.comment || ''}
          onChange={updateCompany('comment')}
          error={!!error?.comment}
          helperText={error?.comment || ''}
          FormHelperTextProps={{
            classes: { root: classes.ErrorHelperText },
          }}
        />
      </DialogContent>
      <FormDialogActions
        loading={loading}
        handleSubmit={handleSubmit}
        handleClose={closeDialog}
      />
    </Dialog>
  );

  function onCompletedMutationHandler({ updateCompany }) {
    if (updateCompany.isSuccess) {
      closeDialog();
    } else {
      if (updateCompany.errors.length) {
        setError(arrToObj(updateCompany.errors));
      }
    }
  }

  function updateCache(cache, { data }) {
    if (company._id || !data.updateCompany.isSuccess) {
      return;
    }
    const items = cache.readQuery({ query: GET_COMPANIES });
    const companies = items.companies ? items.companies.data : [];

    cache.writeQuery({
      query: GET_COMPANIES,
      data: {
        companies: {
          ...items.companies,
          data: [...companies, ...data.updateCompany.data],
        },
      },
    });
  }
}

export default CompanyDialog;
