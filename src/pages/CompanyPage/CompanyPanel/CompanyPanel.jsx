import React from 'react';
import { Grid, Button, Typography } from '@material-ui/core';

import { useCompanyPageContext } from '../companyPageContext';

import useStyles from './styles';

function CompanyPanel() {
  const classes = useStyles();
  const { openDialog } = useCompanyPageContext();

  return (
    <Grid
      container
      alignItems='flex-end'
      spacing={2}
      classes={{ root: classes.GridRoot }}
    >
      <Grid item>
        <Button variant='contained' color='secondary' onClick={openDialog}>
          Добавить
        </Button>
      </Grid>
      <Grid item xs>
        <Typography variant='h4' align='right'>
          Организации
        </Typography>
      </Grid>
    </Grid>
  );
}

export default CompanyPanel;
