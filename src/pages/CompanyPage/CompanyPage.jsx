import React from 'react';
import { Container } from '@material-ui/core';
import CompanyPanel from './CompanyPanel';
import CompanyContent from './CompanyContent';
import CompanyDialog from './CompanyDialog';

import { CompanyPageProvider } from './companyPageContext';

function CompanyPage() {
  return (
    <Container maxWidth='md' disableGutters>
      <CompanyPageProvider>
        <CompanyPanel />
        <CompanyContent />
        <CompanyDialog />
      </CompanyPageProvider>
    </Container>
  );
}

export default CompanyPage;
