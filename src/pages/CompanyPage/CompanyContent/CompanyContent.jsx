import React from 'react';
import { Table, TableContainer, TableHead, TableBody } from '@material-ui/core';
import { TableRow, TableCell } from '@material-ui/core';
import { Paper, Typography } from '@material-ui/core';
import {
  StyledTableCell,
  StyledTableRow,
} from '../../../components/table/StyledTable.jsx';

import { useQuery } from '@apollo/react-hooks';
import { useCompanyPageContext } from '../companyPageContext';
import { GET_COMPANIES } from '../../../graphql/query';

const getTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <StyledTableCell>Название</StyledTableCell>
        <StyledTableCell>Адрес</StyledTableCell>
      </TableRow>
    </TableHead>
  );
};

function CompanyContent() {
  const { setCompany } = useCompanyPageContext();

  const { data, loading } = useQuery(GET_COMPANIES);

  if (loading) {
    return (
      <Typography variant='h6' align='center'>
        loading...
      </Typography>
    );
  }

  const handleClick = (company) => () => {
    setCompany(company);
  };

  return (
    <TableContainer component={Paper}>
      <Table size='small'>
        {getTableHead()}
        <TableBody>
          {sortedCompanies().map((company) => {
            return (
              <StyledTableRow key={company._id} onClick={handleClick(company)}>
                <TableCell style={{ width: '100%' }}>
                  <Typography variant='h5'>{company.title}</Typography>
                </TableCell>
                <TableCell>
                  <Typography noWrap variant='caption'>
                    {company.address}
                  </Typography>
                </TableCell>
              </StyledTableRow>
            );
          })}
        </TableBody>
      </Table>
      {sortedCompanies().length === 0 && (
        <Typography variant='h6' align='center'>
          Нет данных
        </Typography>
      )}
    </TableContainer>
  );

  function sortedCompanies() {
    const companies = data?.companies?.data || [];

    return companies.sort((a, b) =>
      a.title.toLowerCase() > b.title.toLowerCase() ? 1 : -1,
    );
  }
}

export default CompanyContent;
