import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from '../../config';

const MODULE_NAME = `${APP_NAME}/COMPANY`;

const DIALOG_OPEN = `${MODULE_NAME}/DIALOG_OPEN`;
const DIALOG_CLOSE = `${MODULE_NAME}/DIALOG_CLOSE`;
const UPDATE_RECORD = `${MODULE_NAME}/UPDATE_RECORD`;
const SET_ITEM = `${MODULE_NAME}/SET_ITEM`;

const CompanyPageContext = React.createContext();

const emptyCompany = {
  title: '',
  address: '',
  comment: '',
};

const ReducerRecord = Record({
  isOpenDialog: false,
  company: emptyCompany,
});

/**
 ** reducer
 */
function companyPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case DIALOG_OPEN: {
      return state.set('isOpenDialog', true);
    }
    case DIALOG_CLOSE: {
      return state.set('isOpenDialog', false).set('company', emptyCompany);
    }
    case UPDATE_RECORD: {
      return state.setIn(['company', payload.key], payload.value);
    }
    case SET_ITEM: {
      return state.set('company', payload.company).set('isOpenDialog', true);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 ** provider
 */
function CompanyPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    companyPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <CompanyPageContext.Provider value={value} {...props} />;
}

/**
 ** context
 */
function useCompanyPageContext() {
  const context = React.useContext(CompanyPageContext);
  if (!context) {
    throw new Error(
      `useCompanyPageContext must be used within a CompanyPageProvider`,
    );
  }
  const [state, dispatch] = context;

  const openDialog = () => dispatch({ type: DIALOG_OPEN });
  const closeDialog = () => dispatch({ type: DIALOG_CLOSE });
  const updateCompany = (key) => (e) =>
    dispatch({ type: UPDATE_RECORD, payload: { key, value: e.target.value } });
  const setCompany = (company) =>
    dispatch({ type: SET_ITEM, payload: { company } });

  return {
    state,
    openDialog,
    closeDialog,
    updateCompany,
    setCompany,
  };
}

export { CompanyPageProvider, useCompanyPageContext };
