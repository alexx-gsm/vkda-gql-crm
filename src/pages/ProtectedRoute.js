import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { useQuery } from '@apollo/react-hooks';
import { IS_LOGGED_IN } from '../graphql/query';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const { data = {}, loading } = useQuery(IS_LOGGED_IN);

  if (loading) {
    return '';
  }

  return (
    <Route
      {...rest}
      render={(props) =>
        data.isLoggedIn ? <Component {...props} /> : <Redirect to='/signin' />
      }
    />
  );
};

export default ProtectedRoute;
