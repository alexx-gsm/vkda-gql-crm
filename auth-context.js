// TODO

import React from 'react';
import { Record } from 'immutable';
import { APP_NAME } from './src/config';

import useLocalStorage from './src/hooks/useLocalStorage';
import { useMutation, useQuery, useApolloClient } from '@apollo/react-hooks';
import { SIGNIN_USER } from './src/graphql/mutation';

const MODULE_NAME = `${APP_NAME}/AUTH`;

const SIGN_IN = `${MODULE_NAME}/SIGN_IN`;
const SIGN_OUT = `${MODULE_NAME}/SIGN_OUT`;

const ReducerRecord = Record({
  isLoggedIn: false,
  user: {},
  loading: false,
});

function authPageReducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case SIGN_IN: {
      return state.setIn(['category', payload.key], payload.value);
    }
    default: {
      throw new Error(`Unsupported action type: ${action.type}`);
    }
  }
}

/**
 * provider
 */
function AuthPageProvider(props) {
  const [state, dispatch] = React.useReducer(
    authPageReducer,
    new ReducerRecord(),
  );
  const value = React.useMemo(() => [state, dispatch], [state]);
  return <AuthPageContext.Provider value={value} {...props} />;
}

const AuthPageContext = React.createContext();

function useAuthPageContext() {
  const context = React.useContext(AuthPageContext);
  if (!context) {
    throw new Error(
      `useAuthPageContext must be used within an AuthPageReducer`,
    );
  }
  const [state, dispatch] = context;

  const [login, { loading: loadingLogin, error }] = useMutation(SIGNIN_USER, {
    onError(error) {
      console.log('login error', error);
    },
  });

  const signIn = async ({ email, password }) => {
    if (email && password) {
      login({ variables: { email, password } });
    }

    // dispatch({ type: SIGN_IN, payload: { isLoggedIn, user } })
  };

  return {
    state,
    signIn,
  };
}

export { AuthPageProvider, useAuthPageContext };
